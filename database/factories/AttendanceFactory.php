<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Attendance>
 */
class AttendanceFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'employee_id' => 1,
            'in_time' => fake()->dateTimeThisMonth(),
            'out_time' => fake()->dateTimeThisMonth(),
            'punch_count' => fake()->numberBetween(0, 5),
            'late_in' => fake()->randomElement(['yes', 'no']),
            'early_out' => fake()->randomElement(['yes', 'no']),
        ];
    }
}
