<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Employee>
 */
class EmployeeFactory extends Factory
{

    protected static ?string $password;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'emp_id' => fake()->unique()->uuid(),
            'name' => fake()->name(),
            'username' => fake()->userName(),
            'email' => fake()->unique()->safeEmail(),
            'photo' => fake()->imageUrl(100, 100),
            'password' => static::$password ??= Hash::make('123456'), // You may want to customize this
            'phone' => fake()->phoneNumber(),
            'designation' => fake()->jobTitle(),
            'division' => fake()->numberBetween(1, 3),
            'department' => fake()->numberBetween(1, 9),
            'sub_department' => fake()->numberBetween(1, 6),
            'unit' => fake()->numberBetween(1, 6),
            'line_manager' => fake()->numberBetween(1, 10),
            'joining_date' => fake()->dateTimeThisDecade(),
            'confirmed' => fake()->randomElement(['yes', 'no']),
            'confirmation_date' => fake()->dateTimeThisDecade(),
            'shift' => fake()->randomElement(['Morning', 'Afternoon', 'Night']),
            'status' => fake()->randomElement(['active', 'inactive']),
            'role' => fake()->randomElement(['Admin', 'User']),
            'last_login' => fake()->dateTimeThisMonth(),
        ];
    }
}
