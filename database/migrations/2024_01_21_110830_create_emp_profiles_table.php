<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('emp_profiles', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('employee_id');
            $table->string('gender', 10)->nullable()->index();
            $table->string('religion', 50)->nullable()->index();
            $table->date('birthday')->nullable();
            $table->date('official_birthday')->nullable();
            $table->string('blood_group', 5)->nullable()->index();
            $table->string('father_name', 255)->nullable();
            $table->string('mother_name', 255)->nullable();
            $table->string('marital_status', 20)->nullable()->index();
            $table->string('spouse', 255)->nullable();
            $table->integer('children')->nullable();
            $table->string('present_address', 255)->nullable();
            $table->string('permanent_address', 255)->nullable();
            $table->timestamps();

            $table->foreign('employee_id')->references('id')->on('employees')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('emp_profiles');
    }
};
