<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->string('emp_id', 50);
            $table->string('name', 255);
            $table->string('username', 50)->index();
            $table->string('email', 100)->index();
            $table->string('photo', 100)->nullable();
            $table->string('password', 100);
            $table->string('phone', 20)->nullable();
            $table->string('designation', 50)->nullable();
            $table->integer('division')->nullable();
            $table->integer('department')->nullable();
            $table->integer('sub_department')->nullable();
            $table->integer('unit')->nullable();
            $table->integer('line_manager')->nullable();
            $table->datetime('joining_date')->nullable();
            $table->enum('confirmed', ['yes', 'no'])->default('no')->index();
            $table->date('confirmation_date')->nullable();
            $table->string('shift', 50)->nullable();
            $table->enum('status', ['active', 'inactive'])->default('active')->index();
            $table->string('role', 15)->nullable();
            $table->datetime('last_login')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('employees');
    }
};
