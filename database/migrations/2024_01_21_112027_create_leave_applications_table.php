<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('leave_applications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('employee_id');
            $table->string('subject', 512)->nullable();
            $table->text('description')->nullable();
            $table->string('type', 128)->nullable()->index();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->float('days')->nullable();
            $table->enum('is_lwp', ['yes', 'no'])->nullable()->comment('Is approved with leave without pay');
            $table->enum('status', ['pending', 'lm_approved', 'approved_lwp', 'approved'])->default('pending')->comment('lm = Line Manager, lwp = Leave Without Pay')->index();
            $table->unsignedBigInteger('approved_by_lm')->nullable();
            $table->unsignedBigInteger('approved_by_hr')->nullable();
            $table->timestamps();

            $table->foreign('employee_id')->references('id')->on('employees')->onDelete('cascade');
            $table->foreign('approved_by_lm')->references('id')->on('employees');
            $table->foreign('approved_by_hr')->references('id')->on('employees');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('leave_applications');
    }
};
