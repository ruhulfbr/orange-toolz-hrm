<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('emp_meal_subscriptions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('employee_id');
            $table->enum('breakfast', ['yes', 'no'])->default('no');
            $table->enum('lunch', ['yes', 'no'])->default('no');
            $table->enum('beef', ['yes', 'no'])->default('no');
            $table->enum('fish', ['yes', 'no'])->default('no');
            $table->timestamps();

            $table->foreign('employee_id')->references('id')->on('employees')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('emp_meal_subscriptions');
    }
};
