<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('emp_docs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('employee_id');
            $table->string('nid', 50)->nullable()->comment('National ID');
            $table->string('passport', 50)->nullable();
            $table->string('tin', 100)->nullable()->comment('Etin Certificate');
            $table->string('cv', 100)->nullable()->comment('Resume');
            $table->string('app_letter', 100)->nullable()->comment('Appointment Letter');
            $table->timestamps();

            $table->foreign('employee_id')->references('id')->on('employees')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('emp_docs');
    }
};
