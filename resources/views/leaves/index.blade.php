@extends('layouts/contentLayoutMaster') 

@section('title', $title) 

@section('content')

<div class="row">
	<div class="col-12">

		<div class="card">
		    <div class="card-header">
		        <h4 class="card-title">Filter</h4>
		    </div>
		    <div class="card-body">
		        <form action="" method="GET">
					<div class="form-group row mb-0 pb-0">
	                  	<div class="col-md-2 col-sm-4 col-xs-8 d-flex">
	                  		<label for="month" class="col-form-label mr-1"><strong>Month: &nbsp; </strong></label>
	                      	<input id="month" type="text" class="form-control yearmonth-picker" name="att_month" placeholder="MM YY" value="{{ !empty($_GET['att_month']) ? $_GET['att_month'] : date('F Y') }}" required />
	                  	</div>

	                  	<div class="pl-0 col-md-2 col-sm-4 col-xs-8">
	                  		<button type="submit" class="btn btn-success">Submit</button>
	                  	</div>
	              	</div>               	
	             </form>
		    </div>
		</div>

		<div class="card">
		    <div class="card-header">
		        <h4 class="card-title">Leave Summary</h4>
		    </div>
		    <div class="card-body">
		        {{-- Summary Goes here --}}
		    </div>
		</div>

	    <div class="card card-company-table">
	        <div class="card-body p-0">
	            <div class="table-responsive">
	                <table class="table">
	                    <thead>
	                        <tr class="font-weight-bold">
	                            <th>Subject</th>
	                            <th class="text-center">Type</th>
	                            <th class="text-center">Start Date</th>
	                            <th class="text-center">End Date</th>
	                            <th class="text-center">Days</th>
	                            <th class="text-center">Request Time</th>
	                            <th class="text-center">Status</th>
	                            <th class="text-center">Action</th>
	                        </tr>
	                    </thead>
	                    <tbody>

	                    	@if(!empty($leaves) && count($leaves) > 0)
	                    		@foreach($leaves as $row)
	                    			<tr class="font-weight-bold">
	                    				<td>{{ $row->subject }}</td>
	                    				<td class="text-center">{{ $row->type }}</td>
	                    				<td class="text-center">{{ $row->start_date }}</td>
	                    				<td class="text-center">{{ $row->end_date }}</td>
	                    				<td class="text-center">{{ $row->days }}</td>
	                    				<td class="text-center">
	                    					{{ date('Y-m-d H:i:s', strtotime($row->created_at)) }}
	                    				</td>

	                    				<td class="text-center">
                    						@if($row->status == 'pending')
                    							<span class="text-warning">
                    								Waiting for Line Manager
                    							</span>
                    						@elseif($row->status == 'lm_approved')
                    							<span class="text-primary">
                    								Waiting for HR Manager
                    							</span>
                    						@elseif($row->status == 'approved_lwp')
                    							<span class="text-info">
                    								Approved Without Pay
                    							</span>
                    						@else
                    							<span class="text-success">
                    								Approved
                    							</span>
                    						@endif
	                    				</td>

	                    				<td class="text-center">
	                    					<div class="dropdown">
			                                    <button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-toggle="dropdown">
			                                        <i data-feather="more-vertical"></i>
			                                    </button>
			                                    <div class="dropdown-menu">
			                                        <a class="dropdown-item" href="javascript:void(0);">
			                                            <i data-feather="edit-2" class="mr-50"></i>
			                                            <span>Edit</span>
			                                        </a>
			                                        <a class="dropdown-item" href="javascript:void(0);">
			                                            <i data-feather="trash" class="mr-50"></i>
			                                            <span>Delete</span>
			                                        </a>
			                                    </div>
			                                </div>
	                    				</td>

	                    			</tr>
	                    		@endforeach
	                    	@else
	                    		<tr>
	                    			<td class="text-center" colspan="8">No data found</td>
	                    		</tr>
	                    	@endif

	                        
	                    </tbody>
	                </table>
	            </div>
	        </div>
	    </div>
	</div>
</div>

@endsection 

