@extends('layouts/contentLayoutMaster') 

@section('title', $title) 

@section('content')

<div class="row">
	<div class="col-12">
	    <div class="card card-company-table">
	        <div class="card-body p-0">
	            <div class="table-responsive">
	                <table class="table">
	                    <thead>
	                        <tr class="font-weight-bold">
	                            <th>Description</th>
	                            <th>link</th>
	                            <th class="text-center" style="width: 20%;">Status</th>
	                        </tr>
	                    </thead>
	                    <tbody>

	                    	@if(!empty($links))
	                    		@foreach($links as $row)
	                    			<tr class="font-weight-bold {{ $row->status == 'inactive' ? 'text-danger' : '' }}">
	                    				<td>{{ $row->title }}</td>
	                    				<td>
	                    					@if( !empty($row->url) )
	                    						<a href="{{ $row->url }}" target="_blank">{{ $row->url }}</a>
	                    					@endif
	                    				</td>

	                    				<td class="text-center">
                    						@if($row->status == 'active')
                    							<span class="text-success">
                    									Active
                    							</span>
                    						@else
                    							<span class="text-danger">
                    									 Inactive
                    							</span>
                    						@endif
	                    				</td>

	                    			</tr>
	                    		@endforeach
	                    	@else
	                    		<tr>
	                    			<td class="text-center" colspan="3">No data found</td>
	                    		</tr>
	                    	@endif

	                        
	                    </tbody>
	                </table>
	            </div>
	        </div>
	    </div>
	</div>
</div>

@endsection 

