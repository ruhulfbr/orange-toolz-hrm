<section class="app-user-view">
    <div class="row">
        <!-- User Card starts-->
        <div class="col-xl-12 col-lg-8 col-md-7">
            <div class="card user-card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-xl-4 col-lg-12 d-flex flex-column justify-content-between border-container-lg">
                            <div class="user-avatar-section">
                                <div class="d-flex justify-content-start">
                                    <img class="img-fluid rounded" src="{{ Helper::userAvatar() }}" height="104" width="104" alt="User avatar" />
                                    <div class="d-flex flex-column ml-1">
                                        <div class="user-info mb-1">
                                            <h4 class="mb-0">{{ auth()->user()->name }} ({{ auth()->user()->emp_id }})</h4>
                                            <span class="card-text">{{ auth()->user()->designation }}</span>
                                        </div>

                                        @if(empty($hide_edit))
                                        <div class="d-flex flex-wrap">
                                            <a href="{{route('edit-profile')}}" class="btn btn-primary">Edit</a>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            {{-- <div class="d-flex align-items-center user-total-numbers d-none">
                                <div class="d-flex align-items-center mr-2">
                                    <div class="color-box bg-light-primary">
                                        <i data-feather="dollar-sign" class="text-primary"></i>
                                    </div>
                                    <div class="ml-1">
                                        <h5 class="mb-0">23.3k</h5>
                                        <small>Monthly Sales</small>
                                    </div>
                                </div>
                                <div class="d-flex align-items-center">
                                    <div class="color-box bg-light-success">
                                        <i data-feather="trending-up" class="text-success"></i>
                                    </div>
                                    <div class="ml-1">
                                        <h5 class="mb-0">$99.87K</h5>
                                        <small>Annual Profit</small>
                                    </div>
                                </div>
                            </div> --}}

                        </div>
                        <div class="col-xl-4 col-lg-12 mt-2 mt-xl-0">
                            <div class="user-info-wrapper">
                                
                                <div class="d-flex flex-wrap">
                                    <div class="user-info-title">
                                        <i data-feather="mail" class="mr-1"></i>
                                        <span class="card-text user-info-title font-weight-bold mb-0">Username</span>
                                    </div>
                                    <span class="mx-1">:</span>
                                    <p class="card-text mb-0">{{ auth()->user()->username }}</p>
                                </div>

                                <div class="d-flex flex-wrap my-50">
                                    <div class="user-info-title">
                                        <i data-feather="mail" class="mr-1"></i>
                                        <span class="card-text user-info-title font-weight-bold mb-0">Email</span>
                                    </div>
                                    <span class="mx-1">:</span>
                                    <p class="card-text mb-0">{{ auth()->user()->email }}</p>
                                </div>

                                <div class="d-flex flex-wrap my-50">
                                    <div class="user-info-title">
                                        <i data-feather="phone" class="mr-1"></i>
                                        <span class="card-text user-info-title font-weight-bold mb-0">Contact</span>
                                    </div>
                                    <span class="mx-1">:</span>
                                    <p class="card-text mb-0">{{ auth()->user()->phone }}</p>
                                </div>

                                <div class="d-flex flex-wrap my-50">
                                    <div class="user-info-title">
                                        <i data-feather="calendar" class="mr-1"></i>
                                        <span class="card-text user-info-title font-weight-bold mb-0">Joining Date</span>
                                    </div>
                                    <span class="mx-1">:</span>
                                    <p class="card-text mb-0">
                                    	{{ date('Y-m-d', strtotime(auth()->user()->joining_date)) }}
                                    </p>
                                </div>

                                <div class="d-flex flex-wrap my-50">
                                    <div class="user-info-title">
                                        <i data-feather="star" class="mr-1"></i>
                                        <span class="card-text user-info-title font-weight-bold mb-0">Permanent</span>
                                    </div>
                                    <span class="mx-1">:</span>
                                    <p class="card-text mb-0 text-capitalize">
                                    	{{ auth()->user()->confirmed }}
                                    </p>
                                </div>

                                <div class="d-flex flex-wrap my-50">
                                    <div class="user-info-title">
                                        <i data-feather="calendar" class="mr-1"></i>
                                        <span class="card-text user-info-title font-weight-bold mb-0">Permanent Date</span>
                                    </div>
                                    <span class="mx-1">:</span>
                                    <p class="card-text mb-0">
                                    	{{ date('Y-m-d', strtotime(auth()->user()->confirmation_date)) }}
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-4 col-lg-12 mt-2 mt-xl-0">
                            <div class="user-info-wrapper">

                                <div class="d-flex flex-wrap">
                                    <div class="user-info-title">
                                        <i data-feather="box" class="mr-1"></i>
                                        <span class="card-text user-info-title font-weight-bold mb-0">Division</span>
                                    </div>
                                    <span class="mx-1">:</span>
                                    <p class="card-text mb-0">{{ auth()->user()->empDivision->name ?? '' }}</p>
                                </div>

                                <div class="d-flex flex-wrap my-50">
                                    <div class="user-info-title">
                                        <i data-feather="box" class="mr-1"></i>
                                        <span class="card-text user-info-title font-weight-bold mb-0">Department</span>
                                    </div>
                                    <span class="mx-1">:</span>
                                    <p class="card-text mb-0">{{ auth()->user()->empDept->name ?? '' }}</p>
                                </div>

                                <div class="d-flex flex-wrap my-50">
                                    <div class="user-info-title">
                                        <i data-feather="box" class="mr-1"></i>
                                        <span class="card-text user-info-title font-weight-bold mb-0">Sub Department</span>
                                    </div>
                                    <span class="mx-1">:</span>
                                    <p class="card-text mb-0">{{ auth()->user()->empSubDept->name ?? '' }}</p>
                                </div>

                                <div class="d-flex flex-wrap my-50">
                                    <div class="user-info-title">
                                        <i data-feather="box" class="mr-1"></i>
                                        <span class="card-text user-info-title font-weight-bold mb-0">Unit</span>
                                    </div>
                                    <span class="mx-1">:</span>
                                    <p class="card-text mb-0">{{ auth()->user()->empUnit->name ?? '' }}</p>
                                </div>

                                <div class="d-flex flex-wrap my-50">
                                    <div class="user-info-title">
                                        <i data-feather="star" class="mr-1"></i>
                                        <span class="card-text user-info-title font-weight-bold mb-0">Team</span>
                                    </div>
                                    <span class="mx-1">:</span>
                                    <p class="card-text mb-0">{{ auth()->user()->team->name ?? '' }}</p>
                                </div>

                                <div class="d-flex flex-wrap my-50">
                                    <div class="user-info-title">
                                        <i data-feather="user" class="mr-1"></i>
                                        <span class="card-text user-info-title font-weight-bold mb-0">Line Manager</span>
                                    </div>
                                    <span class="mx-1">:</span>
                                    <p class="card-text mb-0">{{ auth()->user()->team->lineManager->name ?? '' }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /User Card Ends-->
    </div>
</section>