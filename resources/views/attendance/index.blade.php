@extends('layouts/contentLayoutMaster') 

@section('title', $title) 

@section('vendor-style')
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
@endsection

@section('content')

<div class="row">
	<div class="col-12">
      <div class="card">
          <div class="card-header">
              <h4 class="card-title">Summary</h4>
          </div>
          <div class="card-body">
              <form action="" method="GET">
									<div class="form-group row mb-0 pb-0">
                  	<div class="col-md-2 col-sm-4 col-xs-8 d-flex">
                  		<label for="month" class="col-form-label mr-1"><strong>Month: &nbsp; </strong></label>
                      	<input id="month" type="text" class="form-control yearmonth-picker" name="att_month" placeholder="MM YY" value="{{ !empty($_GET['att_month']) ? $_GET['att_month'] : date('F Y') }}" required />
                  	</div>

                  	<div class="pl-0 col-md-2 col-sm-4 col-xs-8">
                  		<button type="submit" class="btn btn-success">Submit</button>
                  	</div>
              	</div>               	
              </form>
          </div>
      </div>
  </div>

	<div class="col-12">
	    <div class="card card-company-table">
	        <div class="card-body p-0">
	            <div class="table-responsive">
	                <table class="table table-bordered font-weight-bold">
	                    <thead>
	                        <tr>
	                            <th>Date</th>
	                            <th class="text-center">Day</th>
	                            <th class="text-center">In Time	</th>
	                            <th class="text-center">Out Time</th>
	                            <th class="text-center">Duration</th>
	                            <th class="text-center">Status</th>
	                            <th class="text-center">Action</th>
	                        </tr>
	                    </thead>
	                    <tbody>

	                    	@if(!empty($attendances))

	                    		@foreach($attendances as $date => $row)
	                    			<tr>
	                    				<td>{{ $date }}</td>
	                    				<td class="text-center">{{ $row->day_name }}</td>
	                    				<td class="text-center">
	                    					<span class="{{ $row->late_in == 'yes' ? 'text-danger' : '' }}">
	                    						{{ $row->in_time_f ?? '' }}
	                    					</span>
	                    				</td>

	                    				<td class="text-center">
	                    					<span class="{{ $row->early_out == 'yes' ? 'text-danger' : '' }}">
	                    						{{ $row->out_time_f ?? '' }}
	                    					</span>
	                    				</td>

	                    				<td class="text-center">
	                    					<span class="{{ $row->early_out == 'yes' ? 'text-danger' : '' }}">
	                    						{{ $row->duration ?? '' }}
	                    					</span>
	                    				</td>

	                    				<td class="text-center">
	                    					<span class="text-capitalize {{ $row->status_class }}"> 
	                    						{{ $row->status ?? '' }}
	                    					</span>

	                    					@if($row->half_day == 'yes')
	                    						<br/> <span class="text-warning">Half Day</span>
	                    					@else
		                    					@if($row->late_in == 'yes')
		                    						<br/> <span class="text-warning">Late In</span>
		                    					@elseif($row->early_out == 'yes')
		                    						<br/> <span class="text-warning">Early Out</span>
		                    						<br/> <span class="text-warning">Short Duration</span>
		                    					@elseif($row->late_in == 'yes' && $row->early_out == 'yes')
		                    						<br/> <span class="text-warning">Half Day</span>
		                    					@endif
		                    				@endif

	                    				</td>
	                    				<td class="text-center">
	                                <div class="dropdown">
	                                    <button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-toggle="dropdown">
	                                        <i data-feather="more-vertical"></i>
	                                    </button>
	                                    <div class="dropdown-menu">
	                                        <a class="dropdown-item" href="javascript:void(0);">
	                                            <i data-feather="edit-2" class="mr-50"></i>
	                                            <span>Edit</span>
	                                        </a>
	                                        <a class="dropdown-item" href="javascript:void(0);">
	                                            <i data-feather="trash" class="mr-50"></i>
	                                            <span>Delete</span>
	                                        </a>
	                                    </div>
	                                </div>
	                            </td>
	                    			</tr>
	                    		@endforeach

	                    	@else

	                    		<tr>
	                    			<td class="text-center" colspan="7">No data found</td>
	                    		</tr>

	                    	@endif

	                        
	                    </tbody>
	                </table>
	            </div>
	        </div>
	    </div>
	</div>
</div>

@endsection 

@section('vendor-script')
  <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection

@section('page-script')
  <script type="text/javascript">
  	
  	$(document).ready(function(){
  		$('.yearmonth-picker').flatpickr({
  			dateFormat: "Y-m-d",
		    altInput: true,
		    altFormat: "F Y",
	    });
  	});

  </script>
@endsection
