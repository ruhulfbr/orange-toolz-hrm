@extends('layouts/contentLayoutMaster') 

@section('title', $title) 

@section('content')

@if(empty($team))
	<div class="alert alert-info alert-dismissible fade show" role="alert">
	   <div class="alert-body py-2">
	      <strong>Note !!</strong>
	      <span>You are not included any Team yet, please ask your reporting manager.</span>
	   </div>
	</div>
@else
<div class="row font-weight-bold">
	<div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Line Manager</h4>
            </div>
            <div class="card-body p-0">
                <div class="table-responsive">
	                <table class="table">
	                    <thead>
	                        <tr class="font-weight-bold">
	                            <th>Name</th>
	                            <th>Phone</th>
	                            <th>Designation</th>
	                            <th class="text-center">Blood Group</th>
	                            <th class="text-center">Gender</th>
	                        </tr>
	                    </thead>
	                    <tbody>

	                    	<tr>
	                    		<td>
	                    			@include('employee.mini-profile', ['employee' => $team->lineManager])
	                    		</td>
	                    		<td>
	                    			<i data-feather='phone'></i> {{ $team->lineManager->phone ?? '' }}
	                    		</td>
	                    		<td>{{ $team->lineManager->designation ?? '' }}</td>
	                    		<td class="text-center">{{ $team->lineManager->profile->blood_group ?? '' }}</td>
	                    		<td class="text-capitalize text-center">{{ $team->lineManager->profile->gender ?? '' }}</td>
	                    	</tr>
	                        
	                    </tbody>
	                </table>
	            </div>
            </div>
        </div>
    </div>

    <div class="col-12">
        <div class="card">
            <div class="card-body p-0">
                <div class="table-responsive">
	                <table class="table">
	                    <thead>
	                        <tr class="font-weight-bold">
	                            <th>Name</th>
	                            <th>Phone</th>
	                            <th>Designation</th>
	                            <th class="text-center">Blood Group</th>
	                            <th class="text-center">Gender</th>
	                        </tr>
	                    </thead>
	                    <tbody>

	                    	@php
                    			$members = $team->employees;
                    		@endphp

	                    	@if( !empty($members) && count($members) > 0 )
	                    		@foreach($members as $member)
	                    			<tr>
			                    		<td>
			                    			@include('employee.mini-profile', ['employee' => $member->employee])
			                    		</td>
			                    		<td>
			                    			<i data-feather='phone'></i> {{ $member->employee->phone ?? '' }}
			                    		</td>
			                    		<td>{{ $member->employee->designation }}</td>
			                    		<td class="text-center">{{ $member->employee->profile->blood_group ?? '' }}</td>
			                    		<td class="text-capitalize text-center">{{ $member->employee->profile->gender ?? '' }}</td>
			                    	</tr>
	                    		@endforeach
	                    	@else
	                    		<tr>
	                    			<td colspan="5" class="text-center">No member found</td>
	                    		</tr>
	                    	@endif
	                        
	                    </tbody>
	                </table>
	            </div>
            </div>
        </div>
    </div>
</div>
@endif

@endsection 

