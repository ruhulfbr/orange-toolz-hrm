@extends('layouts/contentLayoutMaster') 

@section('title', $title) 

@section('content')

<div class="row">
	<div class="col-12">
	    <div class="card card-company-table">
	        <div class="card-body p-0">
	            <div class="table-responsive">
	                <table class="table">
	                    <thead>
	                        <tr class="font-weight-bold">
	                            <th style="width: 40%;">Subject</th>
	                            <th class="text-center">Date</th>
	                            <th class="text-center">Day</th>
	                            <th class="text-center">Status</th>
	                        </tr>
	                    </thead>
	                    <tbody>

	                    	@if(!empty($holidays) && count($holidays) > 0)
	                    		@foreach($holidays as $row)
	                    			<tr class="font-weight-bold {{ $row->status == 'inactive' ? 'text-danger' : '' }}">
	                    				<td>{{ $row->name }}</td>
	                    				<td class="text-center">{{ $row->date }}</td>
	                    				<td class="text-center">
	                    						{{ date('l', strtotime($row->date)) }}
	                    				</td>

	                    				<td class="text-center">
                    						@if($row->status == 'active')
                    							<span class="text-success">
                    									Active
                    							</span>
                    						@else
                    							<span class="text-danger">
                    									 Inactive
                    							</span>
                    						@endif
	                    				</td>

	                    			</tr>
	                    		@endforeach
	                    	@else
	                    		<tr>
	                    			<td class="text-center" colspan="4">No data found</td>
	                    		</tr>
	                    	@endif

	                        
	                    </tbody>
	                </table>
	            </div>
	        </div>
	    </div>
	</div>
</div>

@endsection 

