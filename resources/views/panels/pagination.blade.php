<div class="d-flex align-items-center justify-content-between font-weight-bold">
	<div class="page-info">
	    Showing {{ $paginationData->firstItem() }} to {{ $paginationData->lastItem() }} of {{ $paginationData->total() }} entries
	</div>
	{{ $paginationData->links('pagination::bootstrap-4') }}
</div>