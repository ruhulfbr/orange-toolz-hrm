<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-left mb-0">@yield('title')</h2>
                <div class="breadcrumb-wrapper">

                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                           <a href="{{ route('home') }}">Dashboard</a>
                        </li>

                        @if( !empty($breadcrumbs) )
                          @foreach ($breadcrumbs as $bread)
                          <li class="breadcrumb-item">
                              @if(!empty($bread['link']))
                              <a href="{{ !empty($bread['link']) ? $bread['link'] : 'javascript:void(0)' }}">
                                  {{ $bread['name'] }}
                              </a>
                              @else
                                {{ $bread['name'] }}
                              @endif
                          </li>
                          @endforeach
                        @endif
                    </ol>

                </div>
            </div>
        </div>
    </div>
</div>
