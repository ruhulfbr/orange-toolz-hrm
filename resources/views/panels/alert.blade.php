@if ($errors->any())
 <div class="alert alert-danger alert-dismissible fade show" role="alert">
      <div class="alert-body px-3">
         <strong>Error!!</strong> Something went wrong, please check the following errors.
      </div>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
@endif

@if (session()->has('error_msg'))
<div class="alert alert-danger alert-dismissible fade show" role="alert">
   <div class="alert-body">
      <strong>Error!!</strong>
      <span>{{ session()->get('error_msg') }}</span>
   </div>
   <button type="button" class="close" data-dismiss="alert" aria-label="Close">
       <span aria-hidden="true">&times;</span>
   </button>
</div>
 @endif

@if (session()->has('warning_msg'))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
   <div class="alert-body">
      <strong>Warning!!</strong>
      <span>{{ session()->get('warning_msg') }}</span>
   </div>
   <button type="button" class="close" data-dismiss="alert" aria-label="Close">
       <span aria-hidden="true">&times;</span>
   </button>
</div>
 @endif
 
 @if (session()->has('success_msg'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
      <div class="alert-body">
         <strong>Success!!</strong>
         <span>{{ session()->get('success_msg') }}</span>
      </div>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
 @endif