@php
    $configData = Helper::applClasses();
@endphp
<div class="main-menu menu-fixed {{ $configData['theme'] === 'dark' ? 'menu-dark' : 'menu-light' }} menu-accordion menu-shadow"
    data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto">

                <a class="navbar-brand mt-0" href="{{ route('home') }}">
                    <img src="{{ asset('images/logo/' . env('MAIN_LOGO')) }}" style="width: 130px">
                </a>

            </li>
            <li class="nav-item nav-toggle">
                <a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse">
                    <i class="d-block d-xl-none text-primary toggle-icon font-medium-4" data-feather="x"></i>
                    <i class="d-none d-xl-block collapse-toggle-icon font-medium-4  text-primary" data-feather="disc"
                        data-ticon="disc"></i>
                </a>
            </li>
        </ul>
    </div>
    <div class="shadow-bottom"></div>
    
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class="navigation-header">
                <span data-i18n=""></span>
                <i data-feather="more-horizontal"></i>
            </li>
            <li class="nav-item {{ Helper::isNavActive('home') }}">
                <a class="d-flex align-items-center" href="{{ route('home') }}">
                    <i data-feather="home"></i>
                    <span class="menu-title text-truncate" data-i18n="Dashboard">Dashboard</span>
                </a>
            </li>

            <li class="nav-item {{ Helper::isNavActive(['profile', 'edit-profile']) }}">
                <a class="d-flex align-items-center" href="{{ route('profile') }}">
                    <i data-feather="user"></i>
                    <span class="menu-title text-truncate" data-i18n="Profile">Profile</span>
                </a>
            </li>

            <li class="nav-item {{ Helper::isNavActive('attendance') }}">
                <a class="d-flex align-items-center" href="{{ route('attendance') }}">
                    <i data-feather="clock"></i>
                    <span class="menu-title text-truncate" data-i18n="Dashboard">Attendance</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="d-flex align-items-center" href="javascript:void(0)">
                    <i data-feather="calendar"></i>
                    <span class="menu-title text-truncate" data-i18n="Leaves">Leaves</span>
                </a>
                <ul class="menu-content">
                    <li class="{{ Helper::isNavActive('leaves') }}">
                        <a class="d-flex align-items-center" href="{{ route('leaves') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">List</span></a>
                    </li>
                    <li>
                        <a class="d-flex align-items-center" href="javascript:void(0)"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Preview">Apply</span></a>
                    </li>
                </ul>
            </li>

            <li class="nav-item {{ Helper::isNavActive('team') }}">
                <a class="d-flex align-items-center" href="{{ route('team') }}">
                    <i class="fas fa-people-line"></i>
                    <span class="menu-title text-truncate" data-i18n="Dashboard">Team</span>
                </a>
            </li>

            @if( empty(auth()->user()->role) || auth()->user()->role != 'super_admin')
            <li class="nav-item {{ Helper::isNavActive(['employees', 'employee.show']) }}">
                <a class="d-flex align-items-center" href="{{ route('employees') }}">
                    <i data-feather="users"></i>
                    <span class="menu-title text-truncate" data-i18n="Employees">Employees</span>
                </a>
            </li>
            @endif

            <li class="nav-item {{ Helper::isNavActive('holidays') }}">
                <a class="d-flex align-items-center" href="{{ route('holidays') }}">
                    <i class="fas fa-plane"></i>
                    <span class="menu-title text-truncate" data-i18n="Dashboard">Holidays</span>
                </a>
            </li>

            <li class="nav-item {{ Helper::isNavActive('important-links') }}">
                <a class="d-flex align-items-center" href="{{ route('important-links') }}">
                    <i data-feather="link"></i>
                    <span class="menu-title text-truncate" data-i18n="Dashboard">Important Links</span>
                </a>
            </li>

            @if (!empty(auth()->user()->role) && auth()->user()->role == 'super_admin')
            <li class="navigation-header">
                <span data-i18n="Apps &amp; Pages">{{ Helper::roleString() }}</span>
                <i data-feather="more-horizontal"></i>
            </li>

            <li class="nav-item">
                <a class="d-flex align-items-center" href="javascript:void(0)">
                    <i data-feather="users"></i>
                    <span class="menu-title text-truncate" data-i18n="Employees">Employees</span>
                </a>
                <ul class="menu-content">
                    <li class="{{ Helper::isNavActive(['employees', 'employee.edit']) }}">
                        <a class="d-flex align-items-center" href="{{ route('employees') }}">
                            <i data-feather="circle"></i>
                            <span class="menu-item text-truncate" data-i18n="Manage">Manage</span>
                        </a>
                    </li>

                    <li class="{{ Helper::isNavActive('employee.create') }}">
                        <a class="d-flex align-items-center" href="{{ route('employee.create') }}">
                            <i data-feather="circle"></i>
                            <span class="menu-item text-truncate" data-i18n="New">New</span>
                        </a>
                    </li>

                </ul>
            </li>

            <li class="nav-item">
                <a class="d-flex align-items-center" href="#"><i data-feather="file-text"></i><span class="menu-title text-truncate" data-i18n="Invoice">Reports</span></a>
                <ul class="menu-content">
                    <li>
                        <a class="d-flex align-items-center" href="app-invoice-list.html"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">List</span></a>
                    </li>
                    <li>
                        <a class="d-flex align-items-center" href="app-invoice-preview.html"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Preview">Preview</span></a>
                    </li>
                    <li>
                        <a class="d-flex align-items-center" href="app-invoice-edit.html"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Edit">Edit</span></a>
                    </li>
                    <li>
                        <a class="d-flex align-items-center" href="app-invoice-add.html"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Add">Add</span></a>
                    </li>
                </ul>
            </li>
            @endif
        </ul>
    </div>

</div>
<!-- END: Main Menu-->
