@extends('layouts/contentLayoutMaster')

@section('title', 'Edit Profile')

@section('vendor-style')
  {{-- Vendor Css files --}}
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
  {{-- Page Css files --}}
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/pages/app-user.css')) }}">
@endsection

@section('content')

<!-- User Card -->
    @include('profile.info-card', ['hide_edit' => 1])
<!-- User Card -->

<!-- users edit start -->
<section class="app-user-edit font-weight-bold">
    <div class="row match-height">
        @include('profile.edit-personal-info')
        @include('profile.edit-social-info')
    </div>

    <div class="row match-height">
        @include('profile.edit-official-info')
        @include('profile.edit-meal-info')
    </div>
</section>
<!-- users edit ends -->
@endsection

@section('vendor-script')
  {{-- Vendor js files --}}
  <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection

@section('page-script')
  {{-- Page js files --}}
  <script src="{{ asset(mix('js/scripts/pages/app-user-edit.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/components/components-navs.js')) }}"></script>

  <script type="text/javascript">
      $(document).ready(function(){
          $('#edit_personal_info').validate();
          $('#edit_official_info').validate();
          $('#edit_social_info').validate();
      });
  </script>

@endsection
