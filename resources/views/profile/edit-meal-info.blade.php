<div class="col-md-6 mb-2">
    <div class="card h-100">
      <div class="card-header">
          <h4 class="mb-1">
              <i data-feather="coffee" class="font-medium-4 mr-25"></i>
              <span class="align-middle">Meal Subscription</span>
          </h4>
      </div>
      <div class="card-body">
          <!-- users edit Info form start -->
          <form method="POST" action="{{ route('profile.meal.update') }}" id="edit_personal_info">
              @csrf
              <div class="form-group row align-items-center">
                  <label class="col-sm-3 col-form-label">Breakfast</label>
                  <div class="col-sm-9">
                      <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="br_yes" name="breakfast" class="custom-control-input @error('breakfast') error @endif" value="yes" required checked
                              @if( old('breakfast', $employee->meal->breakfast ?? '') == "yes") checked @endif
                            />
                            <label class="custom-control-label" for="br_yes">Yes</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="br_no" name="breakfast" class="custom-control-input" value="no" required
                                @if( old('breakfast', $employee->meal->breakfast ?? '') == "no") checked @endif
                             />
                            <label class="custom-control-label" for="br_no">No</label>
                        </div>

                        @error('breakfast')
                          <div class="invalid-feedback d-block">{{ $message }}</div>
                        @enderror
                  </div>
              </div>

              <div class="form-group row align-items-center">
                  <label class="col-sm-3 col-form-label">Lunch</label>
                  <div class="col-sm-9">
                      <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="lu_yes" name="lunch" class="custom-control-input @error('lunch') error @endif" value="yes" required checked
                              @if( old('lunch', $employee->meal->lunch ?? '') == "yes") checked @endif
                            />
                            <label class="custom-control-label" for="lu_yes">Yes</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="lu_no" name="lunch" class="custom-control-input" value="no" required
                                @if( old('lunch', $employee->meal->lunch ?? '') == "no") checked @endif
                             />
                            <label class="custom-control-label" for="lu_no">No</label>
                        </div>

                        @error('lunch')
                          <div class="invalid-feedback d-block">{{ $message }}</div>
                        @enderror
                  </div>
              </div>

              <div class="form-group row align-items-center">
                  <label class="col-sm-3 col-form-label">Beef</label>
                  <div class="col-sm-9">
                      <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="be_yes" name="beef" class="custom-control-input @error('beef') error @endif" value="yes" required checked
                              @if( old('beef', $employee->meal->beef ?? '') == "yes") checked @endif
                            />
                            <label class="custom-control-label" for="be_yes">Yes</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="be_no" name="beef" class="custom-control-input" value="no" required
                                @if( old('beef', $employee->meal->beef ?? '') == "no") checked @endif
                             />
                            <label class="custom-control-label" for="be_no">No</label>
                        </div>

                        @error('beef')
                          <div class="invalid-feedback d-block">{{ $message }}</div>
                        @enderror
                  </div>
              </div>

              <div class="form-group row align-items-center">
                  <label class="col-sm-3 col-form-label">Fish</label>
                  <div class="col-sm-9">
                      <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="fi_yes" name="fish" class="custom-control-input @error('fish') error @endif" value="yes" required checked
                              @if( old('fish', $employee->meal->fish ?? '') == "yes") checked @endif
                            />
                            <label class="custom-control-label" for="fi_yes">Yes</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="fi_no" name="fish" class="custom-control-input" value="no" required
                                @if( old('fish', $employee->meal->fish ?? '') == "no") checked @endif
                             />
                            <label class="custom-control-label" for="fi_no">No</label>
                        </div>

                        @error('fish')
                          <div class="invalid-feedback d-block">{{ $message }}</div>
                        @enderror
                  </div>
              </div>

              <div class="row mt-1">
                  <div class="col-12 d-flex flex-sm-row flex-column">
                      <button type="submit" class="btn btn-primary mt-2 mb-sm-0 mr-0">Save Changes</button>
                  </div>
              </div>
          </form>
          <!-- users edit Info form ends -->
      </div>
  </div>
</div>
