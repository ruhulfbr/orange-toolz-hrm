<div class="col-md-6 mb-2">
    <div class="card h-100">
      <div class="card-header">
          <h4 class="mb-1">
              <i data-feather="codesandbox" class="font-medium-4 mr-25"></i>
              <span class="align-middle">Official Information</span>
          </h4>
      </div>
      <div class="card-body">
          <!-- users edit Info form start -->
          <form method="POST" action="{{ route('profile.official-info.update') }}" id="edit_official_info">
              @csrf

              <div class="form-group row">
                  <label for="nid" class="col-sm-3 col-form-label">NID</label>
                  <div class="col-sm-9">
                      <input id="nid" type="text" class="form-control @error('nid') is-invalid @enderror" name="nid" value="{{ old('nid', $employee->official->nid ?? '') }}" required />

                      @error('nid')
                          <div class="invalid-feedback d-block">{{ $message }}</div>
                      @enderror
                  </div>
              </div>

              <div class="form-group row">
                  <label for="tin" class="col-sm-3 col-form-label">TIN</label>
                  <div class="col-sm-9">
                      <input id="tin" type="text" class="form-control @error('tin') is-invalid @enderror" name="tin" value="{{ old('tin', $employee->official->tin ?? '') }}" />
                      
                      @error('tin')
                          <div class="invalid-feedback d-block">{{ $message }}</div>
                      @enderror
                  </div>
              </div>

              <div class="form-group row">
                <label for="skype" class="col-sm-3 col-form-label">Skype</label>
                <div class="col-sm-9">
                    <input id="skype" type="text" class="form-control @error('skype') is-invalid @enderror" name="skype" value="{{ old('skype', $employee->official->skype ?? '') }}" required />

                    @error('skype')
                        <div class="invalid-feedback d-block">{{ $message }}</div>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label for="gmail" class="col-sm-3 col-form-label">Gmail</label>
                <div class="col-sm-9">
                    <input id="gmail" type="email" class="form-control @error('gmail') is-invalid @enderror" name="gmail" value="{{ old('gmail', $employee->official->gmail ?? '') }}" required />
                    @error('gmail')
                        <div class="invalid-feedback d-block">{{ $message }}</div>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label for="gitlab" class="col-sm-3 col-form-label">GitLab</label>
                <div class="col-sm-9">
                    <input id="gitlab" type="text" class="form-control @error('gitlab') is-invalid @enderror" name="gitlab" value="{{ old('gitlab', $employee->official->gitlab ?? '') }}" />
                    @error('gitlab')
                        <div class="invalid-feedback d-block">{{ $message }}</div>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label for="github" class="col-sm-3 col-form-label">GitHub</label>
                <div class="col-sm-9">
                    <input id="github" type="text" class="form-control @error('github') is-invalid @enderror" name="github" value="{{ old('github', $employee->official->github ?? '') }}" />
                    @error('github')
                        <div class="invalid-feedback d-block">{{ $message }}</div>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label for="bank_name" class="col-sm-3 col-form-label">Bank Name</label>
                <div class="col-sm-9">
                    <input id="bank_name" type="text" class="form-control @error('bank_name') is-invalid @enderror" name="bank_name" value="{{ old('bank_name', $employee->official->bank_name ?? '') }}" />
                    
                    @error('bank_name')
                        <div class="invalid-feedback d-block">{{ $message }}</div>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label for="bank_account_no" class="col-sm-3 col-form-label">Bank Account No</label>
                <div class="col-sm-9">
                    <input id="bank_account_no" type="text" class="form-control @error('bank_account_no') is-invalid @enderror" name="bank_account_no" value="{{ old('bank_account_no', $employee->official->bank_account_no ?? '') }}" />
                    @error('bank_account_no')
                        <div class="invalid-feedback d-block">{{ $message }}</div>
                    @enderror
                </div>
            </div>

              <div class="row mt-1">
                  <div class="col-12 d-flex flex-sm-row flex-column">
                      <button type="submit" class="btn btn-primary mt-2 mb-sm-0 mr-0">Save Changes</button>
                  </div>
              </div>
          </form>
          <!-- users edit Info form ends -->
      </div>
  </div>
</div>