@extends('layouts/contentLayoutMaster') 

@section('title', $title) 

@section('page-style') {{-- Page Css files --}}
<link rel="stylesheet" href="{{ asset(mix('css/base/pages/app-user.css')) }}" />
@endsection 

@section('content')

<!-- User Card -->
@include('profile.info-card')
<!-- User Card -->

<section class="app-user-view font-weight-bold">
    <!-- User Timeline & Permissions Starts -->
    <div class="row match-height">
        <!-- information starts -->
        <div class="col-md-6">
		    <div class="card">
			    <div class="card-header">
			        <h4 class="card-title">Meal Subscription</h4>
			    </div>
			    <div class="card-body">
			    	<div class="table-responsive">
				        <table class="table">

				            <tbody>
				                <tr class="border-0">
				                    <td>Breakfast</td>
				                    <td>:</td>
				                    <td class="text-capitalize">{{ $employee->meal->breakfast ?? '' }}</td>
				                </tr>

				                <tr>
				                    <td>Lunch</td>
				                    <td>:</td>
				                    <td class="text-capitalize">{{ $employee->meal->lunch ?? '' }}</td>
				                </tr>

				                <tr>
				                    <td>Beef</td>
				                    <td>:</td>
				                    <td class="text-capitalize">{{ $employee->meal->beef ?? '' }}</td>
				                </tr>

				                <tr>
				                    <td>Fish</td>
				                    <td>:</td>
				                    <td class="text-capitalize">{{ $employee->meal->fish ?? '' }}</td>
				                </tr>
				            </tbody>
				        </table>
				    </div>
			    </div>
			</div>
		</div>

		<div class="col-md-6">
		    <div class="card">
			    <div class="card-header">
			        <h4 class="card-title">Leave Balance</h4>
			    </div>
			    <div class="card-body">
			    	<div class="table-responsive">
				        <table class="table">

				            <tbody>
				                <tr>
				                    <td>Annual Leave</td>
				                    <td>:</td>
				                    <td>0.00 (0.00)</td>
				                </tr>

				                <tr>
				                    <td>Sick Leave</td>
				                    <td>:</td>
				                    <td>0.00 (0.00)</td>
				                </tr>

				                <tr>
				                    <td>Casual Leave</td>
				                    <td>:</td>
				                    <td>0.00 (0.00)</td>
				                </tr>
				            </tbody>
				        </table>
				    </div>
			    </div>
			</div>
		</div>
	</div>

	<div class="row match-height">
		<div class="col-md-6">
		    <div class="card">
			    <div class="card-header">
			        <h4 class="card-title">Personal Information</h4>
			    </div>
			    <div class="card-body">
			    	<div class="table-responsive">
				        <table class="table">

				            <tbody>
				                <tr>
				                    <td>Gender</td>
				                    <td>:</td>
				                    <td class="text-capitalize">{{ $employee->profile->gender ?? '' }}</td>
				                </tr>

				                <tr>
				                    <td>Religion</td>
				                    <td>:</td>
				                    <td class="text-capitalize">{{ $employee->profile->religion ?? '' }}</td>
				                </tr>

				                <tr>
				                    <td>Birthday</td>
				                    <td>:</td>
				                    <td class="text-capitalize">{{ $employee->profile->birth_day ?? '' }}</td>
				                </tr>

				                <tr>
				                    <td>Official Birthday</td>
				                    <td>:</td>
				                    <td class="text-capitalize">{{ $employee->profile->official_birthday ?? '' }}</td>
				                </tr>

				                <tr>
				                    <td>Blood Group</td>
				                    <td>:</td>
				                    <td class="text-capitalize">{{ $employee->profile->blood_group ?? '' }}</td>
				                </tr>

				                <tr>
				                    <td>Father's Name</td>
				                    <td>:</td>
				                    <td class="text-capitalize">{{ $employee->profile->father_name ?? '' }}</td>
				                </tr>

				                <tr>
				                    <td>Mother's Name</td>
				                    <td>:</td>
				                    <td class="text-capitalize">{{ $employee->profile->father_name ?? '' }}</td>
				                </tr>

				                <tr>
				                    <td>Marital Status</td>
				                    <td>:</td>
				                    <td class="text-capitalize">{{ $employee->profile->marital_status ?? '' }}</td>
				                </tr>

				                <tr>
				                    <td>Spouse</td>
				                    <td>:</td>
				                    <td class="text-capitalize">{{ $employee->profile->spouse ?? '' }}</td>
				                </tr>

				                <tr>
				                    <td>Children</td>
				                    <td>:</td>
				                    <td class="text-capitalize">{{ $employee->profile->children ?? '' }}</td>
				                </tr>

				                <tr>
				                    <td>Present Address</td>
				                    <td>:</td>
				                    <td class="text-capitalize">{{ $employee->profile->present_address ?? '' }}</td>
				                </tr>

				                <tr>
				                    <td>Permanent Address</td>
				                    <td>:</td>
				                    <td class="text-capitalize">{{ $employee->profile->permanent_address ?? '' }}</td>
				                </tr>

				            </tbody>
				        </table>
				    </div>
			    </div>
			</div>
		</div>

		<div class="col-md-6">
		    <div class="card">
			    <div class="card-header">
			        <h4 class="card-title">Official Information</h4>
			    </div>
			    <div class="card-body">
			    	<div class="table-responsive">
				        <table class="table">

				            <tbody>
				            	<tr>
				                    <td>NID</td>
				                    <td>:</td>
				                    <td>{{ $employee->official->nid ?? '' }}</td>
				                </tr>
				                <tr>
				                    <td>TIN</td>
				                    <td>:</td>
				                    <td>{{ $employee->official->tin ?? '' }}</td>
				                </tr>
				                <tr>
				                    <td>Bank Name</td>
				                    <td>:</td>
				                    <td>{{ $employee->official->bank_name ?? '' }}</td>
				                </tr>
				                <tr>
				                    <td>Bank Account No</td>
				                    <td>:</td>
				                    <td>{{ $employee->official->bank_account_no ?? '' }}</td>
				                </tr>
				                <tr>
				                    <td>Skype</td>
				                    <td>:</td>
				                    <td>{{ $employee->official->skype ?? '' }}</td>
				                </tr>

				                <tr>
				                    <td>Gmail</td>
				                    <td>:</td>
				                    <td>{{ $employee->official->gmail ?? '' }}</td>
				                </tr>

				                <tr>
				                    <td>Gitlab</td>
				                    <td>:</td>
				                    <td>{{ $employee->official->gitlab ?? '' }}</td>
				                </tr>

				                <tr>
				                    <td>Github</td>
				                    <td>:</td>
				                    <td>{{ $employee->official->github ?? '' }}</td>
				                </tr>
				            </tbody>
				        </table>
				    </div>
			    </div>
			</div>
		</div>
	</div>

	<div class="row match-height">
		<div class="col-md-6">
		    <div class="card">
			    <div class="card-header">
			        <h4 class="card-title">Social Information</h4>
			    </div>
			    <div class="card-body">
			    	<div class="table-responsive">
				        <table class="table">

				            <tbody>

				            	<tr>
				                    <td>Gmail</td>
				                    <td>:</td>
				                    <td>{{ $employee->social->gmail ?? '' }}</td>
				                </tr>

				            	<tr>
				                    <td>Gitlab</td>
				                    <td>:</td>
				                    <td>{{ $employee->social->gitlab ?? '' }}</td>
				                </tr>

				                <tr>
				                    <td>Github</td>
				                    <td>:</td>
				                    <td>{{ $employee->social->github ?? '' }}</td>
				                </tr>
				            	<tr>
				                    <td>Website</td>
				                    <td>:</td>
				                    <td>{{ $employee->social->website ?? '' }}</td>
				                </tr>
				                <tr>
				                    <td>Facebook</td>
				                    <td>:</td>
				                    <td>{{ $employee->social->facebook ?? '' }}</td>
				                </tr>
				                <tr>
				                    <td>Twitter</td>
				                    <td>:</td>
				                    <td>{{ $employee->social->twitter ?? '' }}</td>
				                </tr>
				                <tr>
				                    <td>LinkedIn</td>
				                    <td>:</td>
				                    <td>{{ $employee->social->linkedin ?? '' }}</td>
				                </tr>
				                <tr>
				                    <td>Instagram</td>
				                    <td>:</td>
				                    <td>{{ $employee->social->instagram ?? '' }}</td>
				                </tr>

				                <tr>
				                    <td>YouTube</td>
				                    <td>:</td>
				                    <td>{{ $employee->social->youtube ?? '' }}</td>
				                </tr>

				                <tr>
				                    <td>Tiktok</td>
				                    <td>:</td>
				                    <td>{{ $employee->social->tikTok ?? '' }}</td>
				                </tr>

				                
				            </tbody>
				        </table>
				    </div>
			    </div>
			</div>
		</div>

    </div>
    <!-- User Timeline & Permissions Ends -->

</section>
@endsection 

@section('page-script') {{-- Page js files --}}
	<script src="{{ asset(mix('js/scripts/pages/app-user-view.js')) }}"></script>	
@endsection
