<div class="col-md-6 mb-2">
    <div class="card h-100">
      <div class="card-header">
          <h4 class="mb-1">
              <i data-feather="user" class="font-medium-4 mr-25"></i>
              <span class="align-middle">Personal Information</span>
          </h4>
      </div>
      <div class="card-body">
          <!-- users edit Info form start -->
          <form method="POST" action="{{ route('profile.personal-info.update') }}" id="edit_personal_info">
              @csrf
              <div class="form-group row align-items-center">
                  <label class="col-sm-3 col-form-label">Gender</label>
                  <div class="col-sm-9">
                      <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="male" name="gender" class="custom-control-input @error('gender') error @endif" value="Male" required 
                              @if( old('gender', $employee->profile->gender ?? '') == "Male") checked @endif
                            />
                            <label class="custom-control-label" for="male">Male</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="female" name="gender" class="custom-control-input" value="Female" required
                                @if( old('gender', $employee->profile->gender ?? '') == "Female") checked @endif
                             />
                            <label class="custom-control-label" for="female">Female</label>
                        </div>

                        @error('gender')
                          <div class="invalid-feedback d-block">{{ $message }}</div>
                        @enderror

                  </div>
              </div>

              <div class="form-group row">
                  <label for="religion" class="col-sm-3 col-form-label">Religion</label>
                  <div class="col-sm-9">
                      @php
                        $religions = Helper::religions();
                      @endphp

                      <select id="religion" class="form-control @error('religion') error @endif" name="religion" required>
                          <option value="">Select Religion</option>
                            @foreach($religions as $religion)
                                @php
                                  $selected = old('religion', $employee->profile->religion ?? '') == $religion ? "selected" : '';
                                @endphp

                                <option value="{{ $religion }}" {{ $selected }} >{{ $religion }}</option>
                            @endforeach
                      </select>

                      @error('religion')
                        <div class="invalid-feedback d-block">{{ $message }}</div>
                      @enderror
                  </div>
              </div>

              <div class="form-group row">
                  <label for="birthday" class="col-sm-3 col-form-label">Birth date</label>
                  <div class="col-sm-9">
                      <input id="birthday" type="text" class="form-control birthdate-picker @error('birthday') error @enderror" name="birthday" placeholder="YYYY-MM-DD" value="{{ old('birthday', $employee->profile->birthday ?? '') }}" required />

                      @error('birthday')
                        <div class="invalid-feedback d-block">{{ $message }}</div>
                      @enderror
                  </div>
              </div>

              <div class="form-group row">
                  <label for="official_birthday" class="col-sm-3 col-form-label">Official Birth date</label>
                  <div class="col-sm-9">
                      <input id="official_birthday" type="text" class="form-control birthdate-picker @error('official_birthday') error @enderror" name="official_birthday" placeholder="YYYY-MM-DD" value="{{ old('official_birthday', $employee->profile->official_birthday ?? '') }}" />

                      @error('official_birthday')
                        <div class="invalid-feedback d-block">{{ $message }}</div>
                      @enderror

                  </div>
              </div>

              <div class="form-group row">
                  <label for="blood_group" class="col-sm-3 col-form-label">Blood Group</label>
                  <div class="col-sm-9">
                      @php
                        $bloodGroups = Helper::bloodBroups();
                      @endphp

                      <select id="blood_group" class="form-control @error('blood_group') error @endif" name="blood_group">
                          <option value="">Select Blood Group</option>
                            @foreach($bloodGroups as $blood_group)
                                @php
                                  $selected = old('blood_group', $employee->profile->blood_group ?? '') == $blood_group ? "selected" : '';
                                @endphp

                                <option value="{{ $blood_group }}" {{ $selected }}>{{ $blood_group }}</option>
                            @endforeach
                      </select>

                      @error('blood_group')
                        <div class="invalid-feedback d-block">{{ $message }}</div>
                      @enderror

                  </div>
              </div>

              <div class="form-group row align-items-center">
                  <label class="col-sm-3 col-form-label">Marital Status</label>
                  <div class="col-sm-9">
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="single" name="marital_status" class="custom-control-input @error('marital_status') error @endif" value="Single" required checked 
                              @if( old('gender', $employee->profile->marital_status ?? '') == "Single") checked @endif
                            />
                            <label class="custom-control-label" for="single">Single</label>
                        </div>

                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="married" name="marital_status" class="custom-control-input" value="Married" required 
                              @if( old('marital_status', $employee->profile->marital_status ?? '') == "Married") checked @endif
                            />
                            <label class="custom-control-label" for="married">Married</label>
                        </div>

                        @error('marital_status')
                          <div class="invalid-feedback d-block">{{ $message }}</div>
                        @enderror

                  </div>
              </div>

              <div class="form-group row">
                  <label for="spouse" class="col-sm-3 col-form-label">Spouse</label>
                  <div class="col-sm-9">
                      <input id="spouse" type="text" class="form-control @error('spouse') error @enderror" name="spouse" placeholder="" value="{{ old('spouse', $employee->profile->spouse ?? '') }}" />

                      @error('spouse')
                        <div class="invalid-feedback d-block">{{ $message }}</div>
                      @enderror
                  </div>
              </div>

              <div class="form-group row">
                  <label for="children" class="col-sm-3 col-form-label">Children</label>
                  <div class="col-sm-9">
                      <input id="children" type="number" class="form-control @error('children') error @enderror" name="children" placeholder="" value="{{ old('children', $employee->profile->children ?? '') }}" />

                      @error('children')
                        <div class="invalid-feedback d-block">{{ $message }}</div>
                      @enderror
                  </div>
              </div>

              <div class="form-group row">
                  <label for="father_name" class="col-sm-3 col-form-label">Father's Name</label>
                  <div class="col-sm-9">
                      <input id="father_name" type="text" class="form-control @error('father_name') error @enderror" name="father_name" placeholder="" value="{{ old('father_name', $employee->profile->father_name ?? '') }}" required />

                      @error('father_name')
                        <div class="invalid-feedback d-block">{{ $message }}</div>
                      @enderror
                  </div>
              </div>

              <div class="form-group row">
                  <label for="mother_name" class="col-sm-3 col-form-label">Mother's Name</label>
                  <div class="col-sm-9">
                      <input id="mother_name" type="text" class="form-control @error('mother_name') error @enderror" name="mother_name" placeholder="" value="{{ old('mother_name', $employee->profile->mother_name ?? '') }}" required />

                      @error('mother_name')
                        <div class="invalid-feedback d-block">{{ $message }}</div>
                      @enderror
                  </div>
              </div>

              <div class="form-group row">
                  <label for="present_address" class="col-sm-3 col-form-label">Present Address</label>
                  <div class="col-sm-9">
                      <textarea name="present_address" id="present_address" class="form-control @error('present_address') error @enderror" required>{{ old('present_address', $employee->profile->present_address ?? '') }}</textarea>

                      @error('present_address')
                        <div class="invalid-feedback d-block">{{ $message }}</div>
                      @enderror
                  </div>
              </div>

              <div class="form-group row">
                  <label for="permanent_address" class="col-sm-3 col-form-label">Permanent Address</label>
                  <div class="col-sm-9">
                      <textarea name="permanent_address" id="permanent_address" class="form-control @error('permanent_address') error @enderror" required>{{ old('permanent_address', $employee->profile->permanent_address ?? '') }}</textarea>

                      @error('permanent_address')
                        <div class="invalid-feedback d-block">{{ $message }}</div>
                      @enderror
                  </div>
              </div>

              <div class="row mt-1">
                  <div class="col-12 d-flex flex-sm-row flex-column">
                      <button type="submit" class="btn btn-primary mt-2 mb-sm-0 mr-0">Save Changes</button>
                  </div>
              </div>
          </form>
          <!-- users edit Info form ends -->
      </div>
  </div>
</div>
