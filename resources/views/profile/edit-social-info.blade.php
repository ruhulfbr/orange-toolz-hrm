<div class="col-md-6 mb-2">
    <div class="card h-100">
      <div class="card-header">
          <h4 class="mb-1">
              <i data-feather='share-2' class="font-medium-4 mr-25"></i>
              <span class="align-middle">Social Information</span>
          </h4>
      </div>
      <div class="card-body">
          <!-- users edit Info form start -->
            <form method="POST" action="{{ route('profile.social-info.update') }}" id="edit_social_info">
                @csrf

                <div class="form-group row">
                    <label for="website" class="col-sm-3 col-form-label">Website</label>
                    <div class="col-sm-9">
                        <input id="website" type="url" class="form-control @error('website') is-invalid @enderror" name="website" value="{{ old('website', $employee->social->website ?? '') }}" />
                        
                        @error('website')
                            <div class="invalid-feedback d-block">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="linkedin" class="col-sm-3 col-form-label">LinkedIn</label>
                    <div class="col-sm-9">
                        <input id="linkedin" type="url" class="form-control @error('linkedin') is-invalid @enderror" name="linkedin" value="{{ old('linkedin', $employee->social->linkedin ?? '') }}" required />
                        
                        @error('linkedin')
                            <div class="invalid-feedback d-block">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="facebook" class="col-sm-3 col-form-label">Facebook</label>
                    <div class="col-sm-9">
                        <input id="facebook" type="url" class="form-control @error('facebook') is-invalid @enderror" name="facebook" value="{{ old('facebook', $employee->social->facebook ?? '') }}" required />
                        
                        @error('facebook')
                            <div class="invalid-feedback d-block">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="instagram" class="col-sm-3 col-form-label">Instagram</label>
                    <div class="col-sm-9">
                        <input id="instagram" type="url" class="form-control @error('instagram') is-invalid @enderror" name="instagram" value="{{ old('instagram', $employee->social->instagram ?? '') }}" />
                        
                        @error('instagram')
                            <div class="invalid-feedback d-block">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="twitter" class="col-sm-3 col-form-label">Twitter</label>
                    <div class="col-sm-9">
                        <input id="twitter" type="url" class="form-control @error('twitter') is-invalid @enderror" name="twitter" value="{{ old('twitter', $employee->social->twitter ?? '') }}" />
                        
                        @error('twitter')
                            <div class="invalid-feedback d-block">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="youtube" class="col-sm-3 col-form-label">Youtube</label>
                    <div class="col-sm-9">
                        <input id="youtube" type="url" class="form-control @error('youtube') is-invalid @enderror" name="youtube" value="{{ old('youtube', $employee->social->youtube ?? '') }}" />
                        
                        @error('youtube')
                            <div class="invalid-feedback d-block">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="tikTok" class="col-sm-3 col-form-label">TikTok</label>
                    <div class="col-sm-9">
                        <input id="tikTok" type="url" class="form-control @error('tikTok') is-invalid @enderror" name="tikTok" value="{{ old('tikTok', $employee->social->tikTok ?? '') }}" />
                        
                        @error('tikTok')
                            <div class="invalid-feedback d-block">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="gmail" class="col-sm-3 col-form-label">Gmail</label>
                    <div class="col-sm-9">
                        <input id="gmail" type="email" class="form-control @error('gmail') is-invalid @enderror" name="gmail" value="{{ old('gmail', $employee->social->gmail ?? '') }}" required />
                        @error('gmail')
                            <div class="invalid-feedback d-block">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="gitlab" class="col-sm-3 col-form-label">GitLab</label>
                    <div class="col-sm-9">
                        <input id="gitlab" type="url" class="form-control @error('gitlab') is-invalid @enderror" name="gitlab" value="{{ old('gitlab', $employee->social->gitlab ?? '') }}" />
                        @error('gitlab')
                            <div class="invalid-feedback d-block">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="github" class="col-sm-3 col-form-label">GitHub</label>
                    <div class="col-sm-9">
                        <input id="github" type="url" class="form-control @error('github') is-invalid @enderror" name="github" value="{{ old('github', $employee->social->github ?? '') }}" />
                        @error('github')
                            <div class="invalid-feedback d-block">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div class="row mt-1">
                    <div class="col-12 d-flex flex-sm-row flex-column">
                        <button type="submit" class="btn btn-primary mt-2 mb-sm-0 mr-0">Save Changes</button>
                    </div>
                </div>
          </form>
          <!-- users edit Info form ends -->
      </div>
  </div>
</div>