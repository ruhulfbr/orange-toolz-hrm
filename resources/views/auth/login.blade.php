@php 
    $configData = Helper::applClasses(); 
@endphp 

@extends('layouts/fullLayoutMaster') 

@section('title', 'Login') 

@section('page-style') {{-- Page Css files --}}
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}" />
<link rel="stylesheet" href="{{ asset(mix('css/base/pages/page-auth.css')) }}" />
@endsection

@section('content')
<div class="auth-wrapper auth-v2">
    <div class="auth-inner row m-0">
        <!-- Brand logo-->
        <a class="brand-logo" href="javascript:void(0);">
            <img src="{{ asset('images/logo/' . env('MAIN_LOGO')) }}" style="width: 180px">
        </a>
        <!-- /Brand logo-->
        <!-- Left Text-->
        <div class="d-none d-lg-flex col-lg-8 align-items-center p-5">
            <div class="w-100 d-lg-flex align-items-center justify-content-center px-5">
                @if($configData['theme'] === 'dark')
                <img class="img-fluid" src="{{asset('images/pages/login-v2-dark.svg')}}" alt="Login V2" />
                @else
                <img class="img-fluid" src="{{asset('images/pages/login-v2.svg')}}" alt="Login V2" />
                @endif
            </div>
        </div>
        <!-- /Left Text-->
        <!-- Login-->
        <div class="d-flex col-lg-4 align-items-center auth-bg px-2 p-lg-5">
            <div class="col-12 col-sm-8 col-md-6 col-lg-12 px-xl-2 mx-auto">
                <h2 class="card-title font-weight-bold mb-1">Welcome to {{ env('APP_NAME') }}! &#x1F44B;</h2>
                <form class="auth-login-form mt-2" method="POST" id="login-form">
                    @csrf
                    <div class="form-group">
                        <label class="form-label" for="username">Email/Username</label>
                        <input class="form-control @error('username') border-danger @enderror" id="username" type="text" name="username" placeholder="john@example.com" aria-describedby="login-email" autofocus="" tabindex="1" required />

                        @error('username')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <div class="d-flex justify-content-between">
                            <label for="login-password">Password</label>
                        </div>
                        <div class="input-group input-group-merge form-password-toggle @error('password') is-invalid @enderror">
                            <input class="form-control form-control-merge @error('password') border-danger @enderror" id="login-password" type="password" name="password" placeholder="············" aria-describedby="login-password" tabindex="2" required />
                            <div class="input-group-append">
                                <span class="input-group-text cursor-pointer">
                                    <i data-feather="eye"></i>
                                </span>
                            </div>
                        </div>

                        @error('password')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary btn-block" tabindex="4">Sign in</button>
                </form>
            </div>
        </div>
        <!-- /Login-->
    </div>
</div>
@endsection 

@section('vendor-script')
<script src="{{asset(mix('vendors/js/forms/validation/jquery.validate.min.js'))}}"></script>
@endsection 

@section('page-script')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#login-form').validate();
        });

    </script>
@endsection
