@extends('layouts/contentLayoutMaster') 

@section('title', $title) 

@section('page-style') {{-- Page Css files --}}
<link rel="stylesheet" href="{{ asset(mix('css/base/pages/app-user.css')) }}" />
@endsection 

@section('content')

<!-- User Card -->
@include('employee.info-card')
<!-- User Card -->

<section class="app-user-view font-weight-bold">
	<div class="row match-height">
		<div class="col-md-6">
		    <div class="card h-100">
			    <div class="card-header">
			        <h4 class="card-title">Personal Information</h4>
			    </div>
			    <div class="card-body">
			    	<div class="table-responsive">
				        <table class="table">

				            <tbody>
				                <tr>
				                    <td>Gender</td>
				                    <td>:</td>
				                    <td class="text-capitalize">{{ $employee->profile->gender ?? '' }}</td>
				                </tr>

				                <tr>
				                    <td>Religion</td>
				                    <td>:</td>
				                    <td class="text-capitalize">{{ $employee->profile->religion ?? '' }}</td>
				                </tr>

				                <tr>
				                    <td>Birthday</td>
				                    <td>:</td>
				                    <td class="text-capitalize">{{ $employee->profile->birth_day ?? '' }}</td>
				                </tr>

				                <tr>
				                    <td>Official Birthday</td>
				                    <td>:</td>
				                    <td class="text-capitalize">{{ $employee->profile->official_birthday ?? '' }}</td>
				                </tr>

				                <tr>
				                    <td>Blood Group</td>
				                    <td>:</td>
				                    <td class="text-capitalize">{{ $employee->profile->blood_group ?? '' }}</td>
				                </tr>
				            </tbody>
				        </table>
				    </div>
			    </div>
			</div>
		</div>

		<div class="col-md-6">
		    <div class="card h-100">
			    <div class="card-header">
			        <h4 class="card-title">Official Information</h4>
			    </div>
			    <div class="card-body">
			    	<div class="table-responsive">
				        <table class="table">

				            <tbody>
				                <tr>
				                    <td>Skype</td>
				                    <td>:</td>
				                    <td>{{ $employee->official->skype ?? '' }}</td>
				                </tr>

				                <tr>
				                    <td>Gmail</td>
				                    <td>:</td>
				                    <td>{{ $employee->official->gmail ?? '' }}</td>
				                </tr>

				                <tr>
				                    <td>Gitlab</td>
				                    <td>:</td>
				                    <td>{{ $employee->official->gitlab ?? '' }}</td>
				                </tr>

				                <tr>
				                    <td>Github</td>
				                    <td>:</td>
				                    <td>{{ $employee->official->github ?? '' }}</td>
				                </tr>
				            </tbody>
				        </table>
				    </div>
			    </div>
			</div>
		</div>
	</div>

	<div class="row match-height">
		<div class="col-md-6">
		    <div class="card">
			    <div class="card-header">
			        <h4 class="card-title">Social Information</h4>
			    </div>
			    <div class="card-body">
			    	<div class="table-responsive">
				        <table class="table">

				            <tbody>

				            	<tr>
				                    <td>Gmail</td>
				                    <td>:</td>
				                    <td>{{ $employee->social->gmail ?? '' }}</td>
				                </tr>

				            	<tr>
				                    <td>Gitlab</td>
				                    <td>:</td>
				                    <td>{{ $employee->social->gitlab ?? '' }}</td>
				                </tr>

				                <tr>
				                    <td>Github</td>
				                    <td>:</td>
				                    <td>{{ $employee->social->github ?? '' }}</td>
				                </tr>
				            	<tr>
				                    <td>Website</td>
				                    <td>:</td>
				                    <td>{{ $employee->social->website ?? '' }}</td>
				                </tr>
				                <tr>
				                    <td>Facebook</td>
				                    <td>:</td>
				                    <td>{{ $employee->social->facebook ?? '' }}</td>
				                </tr>
				                <tr>
				                    <td>Twitter</td>
				                    <td>:</td>
				                    <td>{{ $employee->social->twitter ?? '' }}</td>
				                </tr>
				                <tr>
				                    <td>LinkedIn</td>
				                    <td>:</td>
				                    <td>{{ $employee->social->linkedin ?? '' }}</td>
				                </tr>
				                <tr>
				                    <td>Instagram</td>
				                    <td>:</td>
				                    <td>{{ $employee->social->instagram ?? '' }}</td>
				                </tr>

				                <tr>
				                    <td>YouTube</td>
				                    <td>:</td>
				                    <td>{{ $employee->social->youtube ?? '' }}</td>
				                </tr>

				                <tr>
				                    <td>Tiktok</td>
				                    <td>:</td>
				                    <td>{{ $employee->social->tikTok ?? '' }}</td>
				                </tr>

				                
				            </tbody>
				        </table>
				    </div>
			    </div>
			</div>
		</div>

    </div>
    <!-- User Timeline & Permissions Ends -->

</section>
@endsection 

@section('page-script') {{-- Page js files --}}
	<script src="{{ asset(mix('js/scripts/pages/app-user-view.js')) }}"></script>	
@endsection
