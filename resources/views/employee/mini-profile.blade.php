<div class="d-flex justify-content-left align-items-center">
    <div class="avatar-wrapper">
        <div class="avatar mr-1">
        	@if(!empty($employee->photo))
                <img src="{{ Helper::userAvatar($employee->photo) }}" alt="Avatar" height="50" width="50">
            @else
            	<img src="https://via.placeholder.com/50x50.png/005511?text=perspiciatis" alt="Avatar" height="59" width="59">
            @endif
        </div>
    </div>
    <div class="d-flex flex-column">
        <a href="{{ route('employee.show', $employee->id) }}" class="user_name text-truncate">
            <span class="font-weight-bolder">{{ $employee->name }}</span>
        </a>											        
        <span class="emp_post font-weight-bold"><i data-feather='mail'></i> {{ $employee->email }}</span>
        <span class="emp_post font-weight-bold"><i class="fas fa-id-card"></i> {{ $employee->emp_id }}</span>
    </div>
</div>