<section class="app-user-view">
    <div class="row">
        <!-- User Card starts-->
        <div class="col-xl-12 col-lg-8 col-md-7">
            <div class="card user-card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-xl-4 col-lg-12 d-flex flex-column justify-content-between border-container-lg">
                            <div class="user-avatar-section">
                                <div class="d-flex justify-content-start">
                                    <img class="img-fluid rounded" src="{{ Helper::userAvatar($employee->photo ?? false) }}" height="104" width="104" alt="User avatar" />
                                    <div class="d-flex flex-column ml-1">
                                        <div class="user-info mb-1">
                                            <h4 class="mb-0">{{ $employee->name }}</h4>
                                            <span class="card-text font-weight-bold"> {{ $employee->designation }}</span><br>
                                            <span class="card-text font-weight-bold"><i data-feather='mail'></i> {{ $employee->email }}</span><br>
                                            <span class="card-text font-weight-bold"><i class="fas fa-id-card"></i> {{ $employee->emp_id }}</span>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-12 mt-2 mt-xl-0">
                            <div class="user-info-wrapper">

                                <div class="d-flex flex-wrap my-50">
                                    <div class="user-info-title">
                                        <i data-feather="phone" class="mr-1"></i>
                                        <span class="card-text user-info-title font-weight-bold mb-0">Contact</span>
                                    </div>
                                    <span class="mx-1">:</span>
                                    <p class="card-text mb-0">{{ $employee->phone }}</p>
                                </div>

                                <div class="d-flex flex-wrap my-50">
                                    <div class="user-info-title">
                                        <i data-feather="star" class="mr-1"></i>
                                        <span class="card-text user-info-title font-weight-bold mb-0">Team</span>
                                    </div>
                                    <span class="mx-1">:</span>
                                    <p class="card-text mb-0">{{ $employee->team->name ?? '' }}</p>
                                </div>

                                <div class="d-flex flex-wrap my-50">
                                    <div class="user-info-title">
                                        <i data-feather="box" class="mr-1"></i>
                                        <span class="card-text user-info-title font-weight-bold mb-0">Division</span>
                                    </div>
                                    <span class="mx-1">:</span>
                                    <p class="card-text mb-0">{{ $employee->empDivision->name ?? '' }}</p>
                                </div>

                            </div>
                        </div>

                        <div class="col-xl-4 col-lg-12 mt-2 mt-xl-0">
                            <div class="user-info-wrapper">
                                <div class="d-flex flex-wrap">
                                    <div class="user-info-title">
                                        <i data-feather="box" class="mr-1"></i>
                                        <span class="card-text user-info-title font-weight-bold mb-0">Department</span>
                                    </div>
                                    <span class="mx-1">:</span>
                                    <p class="card-text mb-0">{{ $employee->empDept->name ?? '' }}</p>
                                </div>

                                <div class="d-flex flex-wrap my-50">
                                    <div class="user-info-title">
                                        <i data-feather="box" class="mr-1"></i>
                                        <span class="card-text user-info-title font-weight-bold mb-0">Sub Department</span>
                                    </div>
                                    <span class="mx-1">:</span>
                                    <p class="card-text mb-0">{{ $employee->empSubDept->name ?? '' }}</p>
                                </div>

                                <div class="d-flex flex-wrap my-50">
                                    <div class="user-info-title">
                                        <i data-feather="box" class="mr-1"></i>
                                        <span class="card-text user-info-title font-weight-bold mb-0">Unit</span>
                                    </div>
                                    <span class="mx-1">:</span>
                                    <p class="card-text mb-0">{{ $employee->empUnit->name ?? '' }}</p>
                                </div>

                                

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /User Card Ends-->
    </div>
</section>