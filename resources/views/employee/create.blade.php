@extends('layouts/contentLayoutMaster')

@section('title', $title)

@section('vendor-style')
  {{-- Vendor Css files --}}
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
  {{-- Page Css files --}}
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
@endsection

@section('content')

<section class="app-user-edit font-weight-bold">
    <div class="row match-height">
        <div class="col-md-6 col-sm-12">
          <div class="card">
            <div class="card-header">
                <h4 class="mb-1">
                    <i data-feather="user" class="font-medium-4 mr-25"></i>
                    <span class="align-middle">New Employee</span>
                </h4>
            </div>
            <div class="card-body">
                <!-- users edit Info form start -->
                <form method="POST" action="{{ route('employee.store') }}" id="new_employee" enctype="multipart/form-data">
                    @csrf

                    <div class="form-group row">
                        <label for="emp_id" class="col-sm-3 col-form-label">Employee Id <span class="text-danger">*</span></label>
                        <div class="col-sm-9">
                            <input id="emp_id" type="text" class="form-control @error('emp_id') error @enderror" name="emp_id" value="{{ old('emp_id') }}" placeholder="001" required />

                            @error('emp_id')
                              <div class="invalid-feedback d-block">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="name" class="col-sm-3 col-form-label">Name <span class="text-danger">*</span></label>
                        <div class="col-sm-9">
                            <input id="name" type="text" class="form-control @error('name') error @enderror" name="name"  value="{{ old('name') }}" placeholder="Jhon" required />

                            @error('name')
                              <div class="invalid-feedback d-block">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="username" class="col-sm-3 col-form-label">Username <span class="text-danger">*</span></label>
                        <div class="col-sm-9">
                            <input id="username" type="text" class="form-control @error('username') error @enderror" name="username"  value="{{ old('username') }}" placeholder="jhon" required />

                            @error('username')
                              <div class="invalid-feedback d-block">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="email" class="col-sm-3 col-form-label">Email <span class="text-danger">*</span></label>
                        <div class="col-sm-9">
                            <input id="email" type="email" class="form-control @error('email') error @enderror" name="email"  value="{{ old('email') }}" placeholder="jhon@example.com" required />

                            @error('email')
                              <div class="invalid-feedback d-block">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password" class="col-sm-3 col-form-label">Password <span class="text-danger">*</span></label>
                        <div class="col-sm-9">
                            <input id="password" type="password" class="form-control @error('password') error @enderror" name="password"  value="{{ old('password') }}" placeholder="****" required />

                            @error('password')
                              <div class="invalid-feedback d-block">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password_confirmation" class="col-sm-3 col-form-label">Confirm Password <span class="text-danger">*</span></label>
                        <div class="col-sm-9">
                            <input id="password_confirmation" type="password" class="form-control @error('password_confirmation') error @enderror" name="password_confirmation"  value="{{ old('password_confirmation') }}" placeholder="****" required />

                            @error('password_confirmation')
                              <div class="invalid-feedback d-block">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="phone" class="col-sm-3 col-form-label">Phone <span class="text-danger">*</span></label>
                        <div class="col-sm-9">
                            <input id="phone" type="number" class="form-control @error('phone') error @enderror" name="phone"  value="{{ old('phone') }}" placeholder="01811000000" required />

                            @error('phone')
                              <div class="invalid-feedback d-block">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="designation" class="col-sm-3 col-form-label">Designation <span class="text-danger">*</span></label>
                        <div class="col-sm-9">
                            <input id="designation" type="text" class="form-control @error('designation') error @enderror" name="designation"  value="{{ old('designation') }}" placeholder="Designation" required />

                            @error('designation')
                              <div class="invalid-feedback d-block">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="team" class="col-sm-3 col-form-label">Team <span class="text-danger">*</span></label>
                        <div class="col-sm-9">
                            <select id="team" class="form-control @error('team') error @endif" name="team" required>
                                <option value="">Select Team</option>
                                  @foreach($teams as $team)
                                      @php
                                        $selected = old('team') == $team->id ? "selected" : '';
                                      @endphp

                                      <option value="{{ $team->id }}" {{ $selected }} >{{ $team->name }} ( {{ $team->lineManager->name }} )</option>
                                  @endforeach
                            </select>

                            @error('team')
                              <div class="invalid-feedback d-block">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="division" class="col-sm-3 col-form-label">Division <span class="text-danger">*</span></label>
                        <div class="col-sm-9">
                            <select id="division" class="form-control @error('division') error @endif" name="division" required>
                                <option value="">Select Division</option>
                                  @foreach($divisions as $division)
                                      @php
                                        $selected = old('division') == $division->id ? "selected" : '';
                                      @endphp

                                      <option value="{{ $division->id }}" {{ $selected }} >{{ $division->name }}</option>
                                  @endforeach
                            </select>

                            @error('division')
                              <div class="invalid-feedback d-block">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="department" class="col-sm-3 col-form-label">Department</label>
                        <div class="col-sm-9">
                            <select id="department" class="form-control @error('department') error @endif" name="department">
                                <option value="">Select Department</option>
                                  @foreach($departments as $department)
                                      @php
                                        $selected = old('department') == $department->id ? "selected" : '';
                                      @endphp

                                      <option value="{{ $department->id }}" {{ $selected }} >{{ $department->name }}</option>
                                  @endforeach
                            </select>

                            @error('department')
                              <div class="invalid-feedback d-block">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="sub_department" class="col-sm-3 col-form-label">Sub Department</label>
                        <div class="col-sm-9">
                            <select id="sub_department" class="form-control @error('sub_department') error @endif" name="sub_department">
                                <option value="">Select Sub Department</option>
                                  @foreach($sub_departments as $sub_department)
                                      @php
                                        $selected = old('sub_department') == $sub_department->id ? "selected" : '';
                                      @endphp

                                      <option value="{{ $sub_department->id }}" {{ $selected }} >{{ $sub_department->name }}</option>
                                  @endforeach
                            </select>

                            @error('sub_department')
                              <div class="invalid-feedback d-block">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="unit" class="col-sm-3 col-form-label">Unit</label>
                        <div class="col-sm-9">
                            <select id="unit" class="form-control @error('unit') error @endif" name="unit">
                                <option value="">Select Unit</option>
                                  @foreach($units as $unit)
                                      @php
                                        $selected = old('unit') == $unit->id ? "selected" : '';
                                      @endphp

                                      <option value="{{ $unit->id }}" {{ $selected }} >{{ $unit->name }}</option>
                                  @endforeach
                            </select>

                            @error('unit')
                              <div class="invalid-feedback d-block">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="joining_date" class="col-sm-3 col-form-label">Joining Date <span class="text-danger">*</span></label>
                        <div class="col-sm-9">
                            <input id="joining_date" type="text" class="form-control birthdate-picker @error('joining_date') error @enderror" name="joining_date" placeholder="YYYY-MM-DD" value="{{ old('joining_date') }}" required />

                            @error('joining_date')
                              <div class="invalid-feedback d-block">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row align-items-center">
                        <label class="col-sm-3 col-form-label">Is Confirmed <span class="text-danger">*</span></label>
                        <div class="col-sm-9">
                            <div class="custom-control custom-radio custom-control-inline">
                                  <input type="radio" id="con_yes" name="confirmed" class="custom-control-input @error('confirmed') error @endif" value="yes" required
                                    @if( old('confirmed') == "yes") checked @endif
                                  />
                                  <label class="custom-control-label" for="con_yes">Yes</label>
                              </div>
                              <div class="custom-control custom-radio custom-control-inline">
                                  <input type="radio" id="con_no" name="confirmed" class="custom-control-input" value="no" required
                                      @if( old('confirmed') == "no") checked @endif  @if( empty(old('confirmed')) ) checked @endif
                                   />
                                  <label class="custom-control-label" for="con_no">No</label>
                              </div>

                              @error('confirmed')
                                <div class="invalid-feedback d-block">{{ $message }}</div>
                              @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="confirmation_date" class="col-sm-3 col-form-label">Confirmation Date</label>
                        <div class="col-sm-9">
                            <input id="confirmation_date" type="text" class="form-control birthdate-picker @error('confirmation_date') error @enderror" name="confirmation_date" placeholder="YYYY-MM-DD" value="{{ old('confirmation_date') }}" />

                            @error('confirmation_date')
                              <div class="invalid-feedback d-block">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="role" class="col-sm-3 col-form-label">Role</label>
                        <div class="col-sm-9">
                            <select id="role" class="form-control @error('role') error @endif" name="role">
                                <option value="">Select Role</option>

                                  @php
                                      $roles = Helper::roles();
                                  @endphp 

                                  @foreach($roles as $value => $role)
                                      @php
                                        $selected = old('role') == $value ? "selected" : '';
                                      @endphp

                                      <option value="{{ $value }}" {{ $selected }} >{{ $role }}</option>
                                  @endforeach
                            </select>

                            @error('role')
                              <div class="invalid-feedback d-block">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="photo" class="col-sm-3 col-form-label">Profile Photo <span class="text-danger">*</span></label>
                        <div class="col-sm-9">
                            <input type="file" class="form-control @error('photo') error @endif" id="photo" name="photo" accept=".jpeg, .jpg, .png, .gif" required />
                              <span class="text-warning">
                                <code class="highlighter-rouge">Allowed types: jpeg,jpg,png,gif and dimension must be: 400*400 px</code>
                              </span>
                              @error('photo')
                                <div class="invalid-feedback d-block">{{ $message }}</div>
                              @enderror
                        </div>
                    </div>

                    <div class="row mt-1">
                        <div class="col-12 d-flex flex-sm-row flex-column">
                            <button type="submit" class="btn btn-primary mt-2 mb-sm-0 mr-0">Submit</button>
                        </div>
                    </div>
                </form>
                <!-- users edit Info form ends -->
            </div>
        </div>
      </div>

    </div>
</section>

@endsection

@section('vendor-script')
  {{-- Vendor js files --}}
  <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection

@section('page-script')
  {{-- Page js files --}}
  <script src="{{ asset(mix('js/scripts/pages/app-user-edit.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/components/components-navs.js')) }}"></script>

  <script type="text/javascript">
      $(document).ready(function(){
          $('#new_employee').validate({
            rules: {
              password: {
                minlength: 6,
                maxlength: 30,
                required: true,
              },
              password_confirmation: {
                equalTo: "#password",
              },
            },
          });
      });
  </script>

@endsection
