@extends('layouts/contentLayoutMaster')

@section('title', $title)

@section('vendor-style')
  {{-- Vendor Css files --}}
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
  {{-- Page Css files --}}
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
@endsection

@section('content')

<section class="app-user-edit font-weight-bold">
    <div class="row match-height">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h4 class="mb-1">
                        <i data-feather="user" class="font-medium-4 mr-25"></i>
                        <span class="align-middle">Edit Employee</span>
                    </h4>
                </div>
                <div class="card-body">
                    <!-- users edit Info form start -->
                    <form method="POST" action="" id="edit_employee" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group row">
                            <label for="emp_id" class="col-sm-3 col-form-label">Employee Id <span class="text-danger">*</span></label>
                            <div class="col-sm-9">
                                <input id="emp_id" type="text" class="form-control @error('emp_id') error @enderror" name="emp_id" value="{{ old('emp_id', $employee->emp_id ?? '') }}" placeholder="001" required />

                                @error('emp_id')
                                  <div class="invalid-feedback d-block">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-sm-3 col-form-label">Name <span class="text-danger">*</span></label>
                            <div class="col-sm-9">
                                <input id="name" type="text" class="form-control @error('name') error @enderror" name="name"  value="{{ old('name', $employee->name ?? '') }}" placeholder="Jhon" required />

                                @error('name')
                                  <div class="invalid-feedback d-block">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="username" class="col-sm-3 col-form-label">Username <span class="text-danger">*</span></label>
                            <div class="col-sm-9">
                                <input id="username" type="text" class="form-control @error('username') error @enderror" name="username"  value="{{ old('username', $employee->username ?? '') }}" placeholder="jhon" required />

                                @error('username')
                                  <div class="invalid-feedback d-block">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-sm-3 col-form-label">Email <span class="text-danger">*</span></label>
                            <div class="col-sm-9">
                                <input id="email" type="email" class="form-control @error('email') error @enderror" name="email"  value="{{ old('email', $employee->email ?? '') }}" placeholder="jhon@example.com" required />

                                @error('email')
                                  <div class="invalid-feedback d-block">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="phone" class="col-sm-3 col-form-label">Phone <span class="text-danger">*</span></label>
                            <div class="col-sm-9">
                                <input id="phone" type="number" class="form-control @error('phone') error @enderror" name="phone"  value="{{ old('phone' , $employee->phone ?? '') }}" placeholder="01811000000" required />

                                @error('phone')
                                  <div class="invalid-feedback d-block">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="designation" class="col-sm-3 col-form-label">Designation <span class="text-danger">*</span></label>
                            <div class="col-sm-9">
                                <input id="designation" type="text" class="form-control @error('designation') error @enderror" name="designation"  value="{{ old('designation', $employee->designation ?? '') }}" placeholder="Designation" required />

                                @error('designation')
                                  <div class="invalid-feedback d-block">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="team" class="col-sm-3 col-form-label">Team <span class="text-danger">*</span></label>
                            <div class="col-sm-9">
                                <select id="team" class="form-control @error('team') error @endif" name="team" required>
                                    <option value="">Select Team</option>
                                      @foreach($teams as $team)
                                          @php
                                            $selected = old('team', $employee->team ?? '') == $team->id ? "selected" : '';
                                          @endphp

                                          <option value="{{ $team->id }}" {{ $selected }} >{{ $team->name }} ( {{ $team->lineManager->name }} )</option>
                                      @endforeach
                                </select>

                                @error('team')
                                  <div class="invalid-feedback d-block">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="division" class="col-sm-3 col-form-label">Division <span class="text-danger">*</span></label>
                            <div class="col-sm-9">
                                <select id="division" class="form-control @error('division') error @endif" name="division" required>
                                    <option value="">Select Division</option>
                                      @foreach($divisions as $division)
                                          @php
                                            $selected = old('division', $employee->division ?? '') == $division->id ? "selected" : '';
                                          @endphp

                                          <option value="{{ $division->id }}" {{ $selected }} >{{ $division->name }}</option>
                                      @endforeach
                                </select>

                                @error('division')
                                  <div class="invalid-feedback d-block">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="department" class="col-sm-3 col-form-label">Department</label>
                            <div class="col-sm-9">
                                <select id="department" class="form-control @error('department') error @endif" name="department">
                                    <option value="">Select Department</option>
                                      @foreach($departments as $department)
                                          @php
                                            $selected = old('department', $employee->department ?? '') == $department->id ? "selected" : '';
                                          @endphp

                                          <option value="{{ $department->id }}" {{ $selected }} >{{ $department->name }}</option>
                                      @endforeach
                                </select>

                                @error('department')
                                  <div class="invalid-feedback d-block">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="sub_department" class="col-sm-3 col-form-label">Sub Department</label>
                            <div class="col-sm-9">
                                <select id="sub_department" class="form-control @error('sub_department') error @endif" name="sub_department">
                                    <option value="">Select Sub Department</option>
                                      @foreach($sub_departments as $sub_department)
                                          @php
                                            $selected = old('sub_department', $employee->sub_department ?? '') == $sub_department->id ? "selected" : '';
                                          @endphp

                                          <option value="{{ $sub_department->id }}" {{ $selected }} >{{ $sub_department->name }}</option>
                                      @endforeach
                                </select>

                                @error('sub_department')
                                  <div class="invalid-feedback d-block">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="unit" class="col-sm-3 col-form-label">Unit</label>
                            <div class="col-sm-9">
                                <select id="unit" class="form-control @error('unit') error @endif" name="unit">
                                    <option value="">Select Unit</option>
                                      @foreach($units as $unit)
                                          @php
                                            $selected = old('unit', $employee->unit ?? '') == $unit->id ? "selected" : '';
                                          @endphp

                                          <option value="{{ $unit->id }}" {{ $selected }} >{{ $unit->name }}</option>
                                      @endforeach
                                </select>

                                @error('unit')
                                  <div class="invalid-feedback d-block">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="joining_date" class="col-sm-3 col-form-label">Joining Date <span class="text-danger">*</span></label>
                            <div class="col-sm-9">
                                <input id="joining_date" type="text" class="form-control birthdate-picker @error('joining_date') error @enderror" name="joining_date" placeholder="YYYY-MM-DD" value="{{ old('joining_date', $employee->joining_date ?? '') }}" required />

                                @error('joining_date')
                                  <div class="invalid-feedback d-block">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row align-items-center">
                            <label class="col-sm-3 col-form-label">Is Confirmed <span class="text-danger">*</span></label>
                            <div class="col-sm-9">
                                <div class="custom-control custom-radio custom-control-inline">
                                      <input type="radio" id="con_yes" name="confirmed" class="custom-control-input @error('confirmed') error @endif" value="yes" required
                                        @if( old('confirmed', $employee->confirmed ?? '') == "yes") checked @endif
                                      />
                                      <label class="custom-control-label" for="con_yes">Yes</label>
                                  </div>
                                  <div class="custom-control custom-radio custom-control-inline">
                                      <input type="radio" id="con_no" name="confirmed" class="custom-control-input" value="no" required
                                          @if( old('confirmed', $employee->confirmed ?? '') == "no") checked @endif
                                       />
                                      <label class="custom-control-label" for="con_no">No</label>
                                  </div>

                                  @error('confirmed')
                                    <div class="invalid-feedback d-block">{{ $message }}</div>
                                  @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="confirmation_date" class="col-sm-3 col-form-label">Confirmation Date</label>
                            <div class="col-sm-9">
                                <input id="confirmation_date" type="text" class="form-control birthdate-picker @error('confirmation_date') error @enderror" name="confirmation_date" placeholder="YYYY-MM-DD" value="{{ old('confirmation_date', $employee->confirmation_date ?? '') }}" />

                                @error('confirmation_date')
                                  <div class="invalid-feedback d-block">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="role" class="col-sm-3 col-form-label">Role</label>
                            <div class="col-sm-9">
                                <select id="role" class="form-control @error('role') error @endif" name="role">
                                    <option value="">Select Role</option>

                                      @php
                                          $roles = Helper::roles();
                                      @endphp 

                                      @foreach($roles as $value => $role)
                                          @php
                                            $selected = old('role', $employee->role ?? '') == $value ? "selected" : '';
                                          @endphp

                                          <option value="{{ $value }}" {{ $selected }} >{{ $role }}</option>
                                      @endforeach
                                </select>

                                @error('role')
                                  <div class="invalid-feedback d-block">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="photo" class="col-sm-3 col-form-label">Profile Photo </label>
                            <div class="col-sm-9">
                                <input type="file" class="form-control @error('photo') error @endif" id="photo" name="photo" accept=".jpeg, .jpg, .png, .gif" />
                                  <span class="text-warning">
                                    <code class="highlighter-rouge">Allowed types: jpeg,jpg,png,gif and dimension must be: 400*400 px</code>
                                  </span>
                                  @error('photo')
                                    <div class="invalid-feedback d-block">{{ $message }}</div>
                                  @enderror

                                <div class="user-avatar-section mt-2">
                                    <div class="d-flex justify-content-start">
                                        <img class="img-fluid rounded" src="{{ Helper::userAvatar($employee->photo) }}" height="104" width="104" alt="{{ $employee->name }}" />
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="row mt-1">
                            <div class="col-12 d-flex flex-sm-row flex-column">
                                <button type="submit" class="btn btn-primary mt-2 mb-sm-0 mr-0">Save Changes</button>
                            </div>
                        </div>
                    </form>
                    <!-- users edit Info form ends -->
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h4 class="mb-1">
                        <i data-feather="user" class="font-medium-4 mr-25"></i>
                        <span class="align-middle">Personal Information</span>
                    </h4>
                </div>
              <div class="card-body">
                  <!-- users edit Info form start -->
                  <form method="POST" action="{{ route('employee.personal-info.update', $employee->id) }}" id="edit_personal_info">
                      @csrf
                      <div class="form-group row align-items-center">
                          <label class="col-sm-3 col-form-label">Gender</label>
                          <div class="col-sm-9">
                              <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="male" name="gender" class="custom-control-input @error('gender') error @endif" value="Male" required 
                                      @if( old('gender', $employee->profile->gender ?? '') == "Male") checked @endif
                                    />
                                    <label class="custom-control-label" for="male">Male</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="female" name="gender" class="custom-control-input" value="Female" required
                                        @if( old('gender', $employee->profile->gender ?? '') == "Female") checked @endif
                                     />
                                    <label class="custom-control-label" for="female">Female</label>
                                </div>

                                @error('gender')
                                  <div class="invalid-feedback d-block">{{ $message }}</div>
                                @enderror

                          </div>
                      </div>

                      <div class="form-group row">
                          <label for="religion" class="col-sm-3 col-form-label">Religion</label>
                          <div class="col-sm-9">
                              @php
                                $religions = Helper::religions();
                              @endphp

                              <select id="religion" class="form-control @error('religion') error @endif" name="religion" required>
                                  <option value="">Select Religion</option>
                                    @foreach($religions as $religion)
                                        @php
                                          $selected = old('religion', $employee->profile->religion ?? '') == $religion ? "selected" : '';
                                        @endphp

                                        <option value="{{ $religion }}" {{ $selected }} >{{ $religion }}</option>
                                    @endforeach
                              </select>

                              @error('religion')
                                <div class="invalid-feedback d-block">{{ $message }}</div>
                              @enderror
                          </div>
                      </div>

                      <div class="form-group row">
                          <label for="birthday" class="col-sm-3 col-form-label">Birth date</label>
                          <div class="col-sm-9">
                              <input id="birthday" type="text" class="form-control birthdate-picker @error('birthday') error @enderror" name="birthday" placeholder="YYYY-MM-DD" value="{{ old('birthday', $employee->profile->birthday ?? '') }}" required />

                              @error('birthday')
                                <div class="invalid-feedback d-block">{{ $message }}</div>
                              @enderror
                          </div>
                      </div>

                      <div class="form-group row">
                          <label for="official_birthday" class="col-sm-3 col-form-label">Official Birth date</label>
                          <div class="col-sm-9">
                              <input id="official_birthday" type="text" class="form-control birthdate-picker @error('official_birthday') error @enderror" name="official_birthday" placeholder="YYYY-MM-DD" value="{{ old('official_birthday', $employee->profile->official_birthday ?? '') }}" />

                              @error('official_birthday')
                                <div class="invalid-feedback d-block">{{ $message }}</div>
                              @enderror

                          </div>
                      </div>

                      <div class="form-group row">
                          <label for="blood_group" class="col-sm-3 col-form-label">Blood Group</label>
                          <div class="col-sm-9">
                              @php
                                $bloodGroups = Helper::bloodBroups();
                              @endphp

                              <select id="blood_group" class="form-control @error('blood_group') error @endif" name="blood_group">
                                  <option value="">Select Blood Group</option>
                                    @foreach($bloodGroups as $blood_group)
                                        @php
                                          $selected = old('blood_group', $employee->profile->blood_group ?? '') == $blood_group ? "selected" : '';
                                        @endphp

                                        <option value="{{ $blood_group }}" {{ $selected }}>{{ $blood_group }}</option>
                                    @endforeach
                              </select>

                              @error('blood_group')
                                <div class="invalid-feedback d-block">{{ $message }}</div>
                              @enderror

                          </div>
                      </div>

                      <div class="form-group row align-items-center">
                          <label class="col-sm-3 col-form-label">Marital Status</label>
                          <div class="col-sm-9">
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="single" name="marital_status" class="custom-control-input @error('marital_status') error @endif" value="Single" required checked 
                                      @if( old('gender', $employee->profile->marital_status ?? '') == "Single") checked @endif
                                    />
                                    <label class="custom-control-label" for="single">Single</label>
                                </div>

                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="married" name="marital_status" class="custom-control-input" value="Married" required 
                                      @if( old('marital_status', $employee->profile->marital_status ?? '') == "Married") checked @endif
                                    />
                                    <label class="custom-control-label" for="married">Married</label>
                                </div>

                                @error('marital_status')
                                  <div class="invalid-feedback d-block">{{ $message }}</div>
                                @enderror

                          </div>
                      </div>

                      <div class="form-group row">
                          <label for="spouse" class="col-sm-3 col-form-label">Spouse</label>
                          <div class="col-sm-9">
                              <input id="spouse" type="text" class="form-control @error('spouse') error @enderror" name="spouse" placeholder="" value="{{ old('spouse', $employee->profile->spouse ?? '') }}" />

                              @error('spouse')
                                <div class="invalid-feedback d-block">{{ $message }}</div>
                              @enderror
                          </div>
                      </div>

                      <div class="form-group row">
                          <label for="children" class="col-sm-3 col-form-label">Children</label>
                          <div class="col-sm-9">
                              <input id="children" type="number" class="form-control @error('children') error @enderror" name="children" placeholder="" value="{{ old('children', $employee->profile->children ?? '') }}" />

                              @error('children')
                                <div class="invalid-feedback d-block">{{ $message }}</div>
                              @enderror
                          </div>
                      </div>

                      <div class="form-group row">
                          <label for="father_name" class="col-sm-3 col-form-label">Father's Name</label>
                          <div class="col-sm-9">
                              <input id="father_name" type="text" class="form-control @error('father_name') error @enderror" name="father_name" placeholder="" value="{{ old('father_name', $employee->profile->father_name ?? '') }}" required />

                              @error('father_name')
                                <div class="invalid-feedback d-block">{{ $message }}</div>
                              @enderror
                          </div>
                      </div>

                      <div class="form-group row">
                          <label for="mother_name" class="col-sm-3 col-form-label">Mother's Name</label>
                          <div class="col-sm-9">
                              <input id="mother_name" type="text" class="form-control @error('mother_name') error @enderror" name="mother_name" placeholder="" value="{{ old('mother_name', $employee->profile->mother_name ?? '') }}" required />

                              @error('mother_name')
                                <div class="invalid-feedback d-block">{{ $message }}</div>
                              @enderror
                          </div>
                      </div>

                      <div class="form-group row">
                          <label for="present_address" class="col-sm-3 col-form-label">Present Address</label>
                          <div class="col-sm-9">
                              <textarea name="present_address" id="present_address" class="form-control @error('present_address') error @enderror" required>{{ old('present_address', $employee->profile->present_address ?? '') }}</textarea>

                              @error('present_address')
                                <div class="invalid-feedback d-block">{{ $message }}</div>
                              @enderror
                          </div>
                      </div>

                      <div class="form-group row">
                          <label for="permanent_address" class="col-sm-3 col-form-label">Permanent Address</label>
                          <div class="col-sm-9">
                              <textarea name="permanent_address" id="permanent_address" class="form-control @error('permanent_address') error @enderror" required>{{ old('permanent_address', $employee->profile->permanent_address ?? '') }}</textarea>

                              @error('permanent_address')
                                <div class="invalid-feedback d-block">{{ $message }}</div>
                              @enderror
                          </div>
                      </div>

                      <div class="row mt-1">
                          <div class="col-12 d-flex flex-sm-row flex-column">
                              <button type="submit" class="btn btn-primary mt-2 mb-sm-0 mr-0">Save Changes</button>
                          </div>
                      </div>
                  </form>
                  <!-- users edit Info form ends -->
              </div>
            </div>
        </div>
    </div>

    <div class="row match-height">
        <div class="col-md-6">
            <div class="card">
              <div class="card-header">
                  <h4 class="mb-1">
                      <i data-feather="codesandbox" class="font-medium-4 mr-25"></i>
                      <span class="align-middle">Official Information</span>
                  </h4>
              </div>
              <div class="card-body">
                  <!-- users edit Info form start -->
                  <form method="POST" action="{{ route('employee.official-info.update', $employee->id) }}" id="edit_official_info">
                      @csrf

                      <div class="form-group row">
                          <label for="nid" class="col-sm-3 col-form-label">NID</label>
                          <div class="col-sm-9">
                              <input id="nid" type="text" class="form-control @error('nid') is-invalid @enderror" name="nid" value="{{ old('nid', $employee->official->nid ?? '') }}" required />

                              @error('nid')
                                  <div class="invalid-feedback d-block">{{ $message }}</div>
                              @enderror
                          </div>
                      </div>

                      <div class="form-group row">
                          <label for="tin" class="col-sm-3 col-form-label">TIN</label>
                          <div class="col-sm-9">
                              <input id="tin" type="text" class="form-control @error('tin') is-invalid @enderror" name="tin" value="{{ old('tin', $employee->official->tin ?? '') }}" />
                              
                              @error('tin')
                                  <div class="invalid-feedback d-block">{{ $message }}</div>
                              @enderror
                          </div>
                      </div>

                      <div class="form-group row">
                        <label for="skype" class="col-sm-3 col-form-label">Skype</label>
                        <div class="col-sm-9">
                            <input id="skype" type="text" class="form-control @error('skype') is-invalid @enderror" name="skype" value="{{ old('skype', $employee->official->skype ?? '') }}" required />

                            @error('skype')
                                <div class="invalid-feedback d-block">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="gmail" class="col-sm-3 col-form-label">Gmail</label>
                        <div class="col-sm-9">
                            <input id="gmail" type="email" class="form-control @error('gmail') is-invalid @enderror" name="gmail" value="{{ old('gmail', $employee->official->gmail ?? '') }}" required />
                            @error('gmail')
                                <div class="invalid-feedback d-block">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="gitlab" class="col-sm-3 col-form-label">GitLab</label>
                        <div class="col-sm-9">
                            <input id="gitlab" type="text" class="form-control @error('gitlab') is-invalid @enderror" name="gitlab" value="{{ old('gitlab', $employee->official->gitlab ?? '') }}" />
                            @error('gitlab')
                                <div class="invalid-feedback d-block">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="github" class="col-sm-3 col-form-label">GitHub</label>
                        <div class="col-sm-9">
                            <input id="github" type="text" class="form-control @error('github') is-invalid @enderror" name="github" value="{{ old('github', $employee->official->github ?? '') }}" />
                            @error('github')
                                <div class="invalid-feedback d-block">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="bank_name" class="col-sm-3 col-form-label">Bank Name</label>
                        <div class="col-sm-9">
                            <input id="bank_name" type="text" class="form-control @error('bank_name') is-invalid @enderror" name="bank_name" value="{{ old('bank_name', $employee->official->bank_name ?? '') }}" />
                            
                            @error('bank_name')
                                <div class="invalid-feedback d-block">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="bank_account_no" class="col-sm-3 col-form-label">Bank Account No</label>
                        <div class="col-sm-9">
                            <input id="bank_account_no" type="text" class="form-control @error('bank_account_no') is-invalid @enderror" name="bank_account_no" value="{{ old('bank_account_no', $employee->official->bank_account_no ?? '') }}" />
                            @error('bank_account_no')
                                <div class="invalid-feedback d-block">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                      <div class="row mt-1">
                          <div class="col-12 d-flex flex-sm-row flex-column">
                              <button type="submit" class="btn btn-primary mt-2 mb-sm-0 mr-0">Save Changes</button>
                          </div>
                      </div>
                  </form>
                  <!-- users edit Info form ends -->
              </div>
          </div>
        </div>

        <div class="col-md-6">
            <div class="card">
              <div class="card-header">
                  <h4 class="mb-1">
                      <i data-feather='share-2' class="font-medium-4 mr-25"></i>
                      <span class="align-middle">Social Information</span>
                  </h4>
              </div>
              <div class="card-body">
                  <!-- users edit Info form start -->
                    <form method="POST" action="{{ route('employee.social-info.update', $employee->id) }}" id="edit_social_info">
                        @csrf

                        <div class="form-group row">
                            <label for="website" class="col-sm-3 col-form-label">Website</label>
                            <div class="col-sm-9">
                                <input id="website" type="url" class="form-control @error('website') is-invalid @enderror" name="website" value="{{ old('website', $employee->social->website ?? '') }}" />
                                
                                @error('website')
                                    <div class="invalid-feedback d-block">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="linkedin" class="col-sm-3 col-form-label">LinkedIn</label>
                            <div class="col-sm-9">
                                <input id="linkedin" type="url" class="form-control @error('linkedin') is-invalid @enderror" name="linkedin" value="{{ old('linkedin', $employee->social->linkedin ?? '') }}" required />
                                
                                @error('linkedin')
                                    <div class="invalid-feedback d-block">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="facebook" class="col-sm-3 col-form-label">Facebook</label>
                            <div class="col-sm-9">
                                <input id="facebook" type="url" class="form-control @error('facebook') is-invalid @enderror" name="facebook" value="{{ old('facebook', $employee->social->facebook ?? '') }}" required />
                                
                                @error('facebook')
                                    <div class="invalid-feedback d-block">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="instagram" class="col-sm-3 col-form-label">Instagram</label>
                            <div class="col-sm-9">
                                <input id="instagram" type="url" class="form-control @error('instagram') is-invalid @enderror" name="instagram" value="{{ old('instagram', $employee->social->instagram ?? '') }}" />
                                
                                @error('instagram')
                                    <div class="invalid-feedback d-block">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="twitter" class="col-sm-3 col-form-label">Twitter</label>
                            <div class="col-sm-9">
                                <input id="twitter" type="url" class="form-control @error('twitter') is-invalid @enderror" name="twitter" value="{{ old('twitter', $employee->social->twitter ?? '') }}" />
                                
                                @error('twitter')
                                    <div class="invalid-feedback d-block">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="youtube" class="col-sm-3 col-form-label">Youtube</label>
                            <div class="col-sm-9">
                                <input id="youtube" type="url" class="form-control @error('youtube') is-invalid @enderror" name="youtube" value="{{ old('youtube', $employee->social->youtube ?? '') }}" />
                                
                                @error('youtube')
                                    <div class="invalid-feedback d-block">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="tikTok" class="col-sm-3 col-form-label">TikTok</label>
                            <div class="col-sm-9">
                                <input id="tikTok" type="url" class="form-control @error('tikTok') is-invalid @enderror" name="tikTok" value="{{ old('tikTok', $employee->social->tikTok ?? '') }}" />
                                
                                @error('tikTok')
                                    <div class="invalid-feedback d-block">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="gmail" class="col-sm-3 col-form-label">Gmail</label>
                            <div class="col-sm-9">
                                <input id="gmail" type="email" class="form-control @error('gmail') is-invalid @enderror" name="gmail" value="{{ old('gmail', $employee->social->gmail ?? '') }}" required />
                                @error('gmail')
                                    <div class="invalid-feedback d-block">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="gitlab" class="col-sm-3 col-form-label">GitLab</label>
                            <div class="col-sm-9">
                                <input id="gitlab" type="url" class="form-control @error('gitlab') is-invalid @enderror" name="gitlab" value="{{ old('gitlab', $employee->social->gitlab ?? '') }}" />
                                @error('gitlab')
                                    <div class="invalid-feedback d-block">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="github" class="col-sm-3 col-form-label">GitHub</label>
                            <div class="col-sm-9">
                                <input id="github" type="url" class="form-control @error('github') is-invalid @enderror" name="github" value="{{ old('github', $employee->social->github ?? '') }}" />
                                @error('github')
                                    <div class="invalid-feedback d-block">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="row mt-1">
                            <div class="col-12 d-flex flex-sm-row flex-column">
                                <button type="submit" class="btn btn-primary mt-2 mb-sm-0 mr-0">Save Changes</button>
                            </div>
                        </div>
                  </form>
                  <!-- users edit Info form ends -->
              </div>
          </div>
        </div>
    </div>

    <div class="row match-height">
        <div class="col-md-6">
            <div class="card">
              <div class="card-header">
                  <h4 class="mb-1">
                      <i data-feather="coffee" class="font-medium-4 mr-25"></i>
                      <span class="align-middle">Meal Subscription</span>
                  </h4>
              </div>
              <div class="card-body">
                  <!-- users edit Info form start -->
                  <form method="POST" action="{{ route('employee.meal.update', $employee->id) }}" id="edit_meal_info">
                      @csrf
                      <div class="form-group row align-items-center">
                          <label class="col-sm-3 col-form-label">Breakfast</label>
                          <div class="col-sm-9">
                              <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="br_yes" name="breakfast" class="custom-control-input @error('breakfast') error @endif" value="yes" required checked
                                      @if( old('breakfast', $employee->meal->breakfast ?? '') == "yes") checked @endif
                                    />
                                    <label class="custom-control-label" for="br_yes">Yes</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="br_no" name="breakfast" class="custom-control-input" value="no" required
                                        @if( old('breakfast', $employee->meal->breakfast ?? '') == "no") checked @endif
                                     />
                                    <label class="custom-control-label" for="br_no">No</label>
                                </div>

                                @error('breakfast')
                                  <div class="invalid-feedback d-block">{{ $message }}</div>
                                @enderror
                          </div>
                      </div>

                      <div class="form-group row align-items-center">
                          <label class="col-sm-3 col-form-label">Lunch</label>
                          <div class="col-sm-9">
                              <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="lu_yes" name="lunch" class="custom-control-input @error('lunch') error @endif" value="yes" required checked
                                      @if( old('lunch', $employee->meal->lunch ?? '') == "yes") checked @endif
                                    />
                                    <label class="custom-control-label" for="lu_yes">Yes</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="lu_no" name="lunch" class="custom-control-input" value="no" required
                                        @if( old('lunch', $employee->meal->lunch ?? '') == "no") checked @endif
                                     />
                                    <label class="custom-control-label" for="lu_no">No</label>
                                </div>

                                @error('lunch')
                                  <div class="invalid-feedback d-block">{{ $message }}</div>
                                @enderror
                          </div>
                      </div>

                      <div class="form-group row align-items-center">
                          <label class="col-sm-3 col-form-label">Beef</label>
                          <div class="col-sm-9">
                              <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="be_yes" name="beef" class="custom-control-input @error('beef') error @endif" value="yes" required checked
                                      @if( old('beef', $employee->meal->beef ?? '') == "yes") checked @endif
                                    />
                                    <label class="custom-control-label" for="be_yes">Yes</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="be_no" name="beef" class="custom-control-input" value="no" required
                                        @if( old('beef', $employee->meal->beef ?? '') == "no") checked @endif
                                     />
                                    <label class="custom-control-label" for="be_no">No</label>
                                </div>

                                @error('beef')
                                  <div class="invalid-feedback d-block">{{ $message }}</div>
                                @enderror
                          </div>
                      </div>

                      <div class="form-group row align-items-center">
                          <label class="col-sm-3 col-form-label">Fish</label>
                          <div class="col-sm-9">
                              <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="fi_yes" name="fish" class="custom-control-input @error('fish') error @endif" value="yes" required checked
                                      @if( old('fish', $employee->meal->fish ?? '') == "yes") checked @endif
                                    />
                                    <label class="custom-control-label" for="fi_yes">Yes</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="fi_no" name="fish" class="custom-control-input" value="no" required
                                        @if( old('fish', $employee->meal->fish ?? '') == "no") checked @endif
                                     />
                                    <label class="custom-control-label" for="fi_no">No</label>
                                </div>

                                @error('fish')
                                  <div class="invalid-feedback d-block">{{ $message }}</div>
                                @enderror
                          </div>
                      </div>

                      <div class="row mt-1">
                          <div class="col-12 d-flex flex-sm-row flex-column">
                              <button type="submit" class="btn btn-primary mt-2 mb-sm-0 mr-0">Save Changes</button>
                          </div>
                      </div>
                  </form>
                  <!-- users edit Info form ends -->
              </div>
          </div>
        </div>

    </div>
</section>

@endsection

@section('vendor-script')
  {{-- Vendor js files --}}
  <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection

@section('page-script')
  {{-- Page js files --}}
  <script src="{{ asset(mix('js/scripts/pages/app-user-edit.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/components/components-navs.js')) }}"></script>

  <script type="text/javascript">
      $(document).ready(function(){
          $('#edit_employee').validate();
          $('#edit_personal_info').validate();
          $('#edit_official_info').validate();
          $('#edit_social_info').validate();
          $('#edit_meal_info').validate();
      });
  </script>

@endsection
