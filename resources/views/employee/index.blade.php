@extends('layouts/contentLayoutMaster') 

@section('title', $title) 

@section('content')

<div class="row font-weight-bold">
    <div class="col-12 font-weight-bold">

    	<div class="card">
            <h5 class="card-header">Search Filter</h5>
            <div class="d-flex justify-content-between align-items-center mx-50 row pt-0 pb-2">
                <div class="col-md-4 user_role"></div>
                <div class="col-md-4 user_plan"></div>
                <div class="col-md-4 user_status"></div>
            </div>
        </div>

        <div class="card">
            <div class="card-body p-0">
                <div class="table-responsive">
	                <table class="table">
	                    <thead>
	                        <tr class="font-weight-bold">
	                            <th>Name</th>
	                            <th>Phone</th>
	                            <th>Designation</th>
	                            <th>Team</th>
	                            <th>Stack</th>
	                            <th class="text-center">Blood G</th>
	                            <th class="text-center">Gender</th>

	                            @if( !empty(auth()->user()->role) )
	                            	<th class="text-center">Action</th>
	                            @endif

	                        </tr>
	                    </thead>
	                    <tbody>

	                    	@if( !empty($employees) && count($employees) > 0 )
	                    		@foreach($employees as $employee)
	                    			<tr>
			                    		<td>
			                    			@include('employee.mini-profile', ['employee' => $employee])
			                    		</td>
			                    		<td>
			                    			<i data-feather='phone'></i> {{ $employee->phone ?? '' }}
			                    		</td>
			                    		<td>{{ $employee->designation }}</td>
			                    		<td>{{ $employee->empTeam->name ?? '' }}</td>
			                    		<td>{{ $employee->empUnit->name ?? '' }}</td>
			                    		<td class="text-center">{{ $employee->profile->blood_group ?? '' }}</td>
			                    		<td class="text-capitalize text-center">{{ $employee->profile->gender ?? '' }}</td>

			                    		@if( !empty(auth()->user()->role) )
			                            	<td class="text-center">
			                            		<div class="dropdown">
				                                    <button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-toggle="dropdown">
				                                        <i data-feather="more-vertical"></i>
				                                    </button>
				                                    <div class="dropdown-menu">
				                                        <a class="dropdown-item" href="{{ route('employee.edit', $employee->id) }}">
				                                            <i data-feather="edit-2" class="mr-50"></i>
				                                            <span>Edit</span>
				                                        </a>
				                                        <a class="dropdown-item" href="{{ route('employee.delete', $employee->id) }}" onclick="return confirmDelete('Are you sure want to delete employee?')">
				                                            <i data-feather="trash" class="mr-50"></i>
				                                            <span>Delete</span>
				                                        </a>
				                                    </div>
				                                </div>
			                            	</td>
			                            @endif
			                    	</tr>
	                    		@endforeach
	                    	@else
	                    		<tr>
	                    			<td colspan="9" class="text-center">No employee found</td>
	                    		</tr>
	                    	@endif
	                        
	                    </tbody>
	                </table>
	            </div>
            </div>

            <div class="card-body mt-1">
            	@include('panels.pagination', ['paginationData' => $employees])
            </div>
        </div>
    </div>
</div>

@endsection 

