<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    use HasFactory;

    protected $indexes = [
        'line_manager',
        'status',
    ];

    public function lineManager()
    {
        return $this->belongsTo(Employee::class, 'line_manager');
    }

    public function employees()
    {
        return $this->hasMany(TeamsEmployee::class, 'team_id');
    }
}
