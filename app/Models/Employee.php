<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable;


class Employee extends Model implements Authenticatable
{
    use HasFactory;
    use \Illuminate\Auth\Authenticatable;

    protected $fillable = [
        'emp_id', 'name', 'username', 'email', 'photo', 'password', 'phone',
        'designation', 'division', 'department', 'sub_department', 'unit',
        'line_manager', 'joining_date', 'confirmed', 'confirmation_date',
        'status', 'role', 'team', 'line_manager',
    ];


    protected $hidden = [
        'password',    
    ];

    protected $casts = [
        'password' => 'hashed',
    ];

    protected $indexes = [
        'username',
        'email',
        'designation',
        'confirmed',
        'status',
    ];

    public function profile()
    {
        return $this->hasOne(EmpProfile::class, 'employee_id');
    }

    public function official()
    {
        return $this->hasOne(EmpOfficialInfo::class, 'employee_id');
    }

    public function social()
    {
        return $this->hasOne(EmpSocialInfo::class, 'employee_id');
    }

    public function meal()
    {
        return $this->hasOne(EmpMealSubscription::class, 'employee_id');
    }

    public function empDivision()
    {
        return $this->belongsTo(Division::class, 'division');
    }

    public function empDept()
    {
        return $this->belongsTo(Department::class, 'department');
    }

    public function empSubDept()
    {
        return $this->belongsTo(SubDepartment::class, 'sub_department');
    }

    public function empUnit()
    {
        return $this->belongsTo(Unit::class, 'unit');
    }

    public function empTeam()
    {
        return $this->belongsTo(Team::class, 'team');
    }
}
