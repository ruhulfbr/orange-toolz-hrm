<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LeaveApplication extends Model
{
    use HasFactory;

    protected $indexes = [
        'employee_id',
        'type',
        'status',
    ];

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function lineManager()
    {
        return $this->belongsTo(Employee::class, 'approved_by_lm');
    }

    public function hr()
    {
        return $this->belongsTo(Employee::class, 'approved_by_hr');
    }
}
