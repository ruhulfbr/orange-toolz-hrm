<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmpProfile extends Model
{
    use HasFactory;

     protected $indexes = [
        'employee_id',
        'gender',
        'blood_group',
        'religion',
        'marital_status'
    ];

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }
}
