<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employee;
use App\Models\LeaveApplication as Leave;

class LeavesController extends Controller
{
    private $_VIEW_PATH = "leaves.";

    public function index(Request $request)
    {
        $leaves = Leave::where('employee_id', auth()->user()->id)
                        ->whereYear('start_date', date('Y'))
                        ->oldest()->get();
                        
        $data = [
            'title'       => 'Leaves',
            'breadcrumbs' => [['name' => 'Leaves']],
            'leaves'      => $leaves
        ];

        return view($this->_VIEW_PATH.'index', $data);
    }
}
