<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;

use Illuminate\Http\Request;
use App\Http\Requests\EmployeeStoreRequest;
use App\Http\Requests\EmployeeUpdateRequest;
use App\Http\Requests\ProfilePersonalInfo;
use App\Http\Requests\ProfileOfficialInfo;
use App\Http\Requests\ProfileSocialInfo;
use App\Http\Requests\ProfleMealSubscription;

use App\Models\Employee;
use App\Models\Division;
use App\Models\Department;
use App\Models\SubDepartment;
use App\Models\Unit;
use App\Models\Team;
use App\Models\EmpProfile;
use App\Models\EmpSocialInfo;
use App\Models\EmpMealSubscription;
use App\Models\EmpOfficialInfo;

class EmployeeController extends Controller
{
    private $_VIEW_PATH = "employee.";
    private $_PF_PATH = "media/profile_photo";
    private $_DOC_PATH = "media/docs/";

    public function index(Request $request)
    {
        $data = [
            'title'       => 'Employees',
            'breadcrumbs' => [['name' => 'Employees']],
            'employees'   => Employee::oldest()->paginate(20)
        ];

        return view($this->_VIEW_PATH.'index', $data);
    }

    public function show($id)
    {
        $employee = Employee::findOrFail($id);

        $data = [
            'title'       => 'Employee Profile',
            'breadcrumbs' => [['name' => 'Employees', 'link' => route('employees')], ['name' => 'Profile']],
            'employee'   => $employee
        ];

        return view($this->_VIEW_PATH.'show', $data);
    }

    public function create()
    {
        $data = [
            'title'       => 'New Employee',
            'breadcrumbs' => [['name' => 'Employees', 'link' => route('employees')], ['name' => 'New Employee']],
            'divisions'    => Division::all(),
            'departments'  => Department::all(),
            'sub_departments' => SubDepartment::all(),
            'units' => Unit::all(),
            'teams' => Team::all()
        ];

        return view($this->_VIEW_PATH.'create', $data);
    }

    public function store(EmployeeStoreRequest $request)
    {
        try{
            $validatedData = $request->validated();
            $validatedData['password'] = \Hash::make($request->password);

            // Upload photo if provided
            if ($request->hasFile('photo')) {
                $orFileName  = $request->file('photo')->getClientOriginalName();
                $filename    = pathinfo($orFileName, PATHINFO_FILENAME);
                $ext         = $request->file('photo')->getClientOriginalExtension();
                $newFileName = time().'.'.$ext;

                request()->photo->move(public_path($this->_PF_PATH), $newFileName);
                $validatedData['photo'] = $newFileName;
            }

            // Create new employee
            $employee = Employee::create($validatedData);

            return redirect()->route('employees')->with(['success_msg' => 'New employee has been created.']);
        }
        catch(\Exception $e){

            return redirect()->back()->withInput()->with(['error_msg' => 'Something went wrong, failed to create new employee.']);
        }
    }

    public function edit($id)
    {
        $employee = Employee::findOrFail($id);

        $data = [
            'title'       => 'Edit Employee Info',
            'breadcrumbs' => [['name' => 'Employees', 'link' => route('employees')], ['name' => 'Edit']],
            'employee'   => $employee,
            'divisions'    => Division::all(),
            'departments'  => Department::all(),
            'sub_departments' => SubDepartment::all(),
            'units' => Unit::all(),
            'teams' => Team::all()
        ];

        return view($this->_VIEW_PATH.'edit', $data);
    }

    public function update(EmployeeUpdateRequest $request, $id)
    {

        try {
            $employee = Employee::findOrFail($id);

            $validatedData = $request->validated();

            if ($request->hasFile('photo')) {

               if ($employee->photo) {
                    $filePath = public_path($this->_PF_PATH . '/' . $employee->photo);
                    if (file_exists($filePath)) {
                        unlink($filePath);
                    }
                }

                $orFileName  = $request->file('photo')->getClientOriginalName();
                $filename    = pathinfo($orFileName, PATHINFO_FILENAME);
                $ext         = $request->file('photo')->getClientOriginalExtension();
                $newFileName = time().'.'.$ext;

                request()->photo->move(public_path($this->_PF_PATH), $newFileName);
                $validatedData['photo'] = $newFileName;
                
            }
            else {
                $validatedData['photo'] = $employee->photo;
            }

            $employee->update($validatedData);

            return redirect()->route('employees')->with(['success_msg' => 'Employee has been updated.']);
        }
        catch (\Exception $e) {
            return redirect()->back()->withInput()->with(['error_msg' => 'Something went wrong, failed to update the employee.']);
        }
    }

    public function updatePersonalInfo(ProfilePersonalInfo $request, $id){
        try{
            $data = $request->validated();
            EmpProfile::updateOrInsert(['employee_id' => $id], $data);

            return redirect()->route('employees')->with(['success_msg' => 'Personal information has been updated.']);
        }
        catch (\Exception $e) {
            return redirect()->back()->withInput()->with(['error_msg' => 'Something went wrong, failed to update.']);
        }
    }

    public function updateOfficialInfo(ProfileOfficialInfo $request, $id){

        try{
            $data = $request->validated();
            EmpOfficialInfo::updateOrInsert(['employee_id' => $id], $data);

            return redirect()->route('employees')->with(['success_msg' => 'Official information has been updated.']);
        }
        catch (\Exception $e) {
            return redirect()->back()->withInput()->with(['error_msg' => 'Something went wrong, failed to update.']);
        }
    }

    public function updateSocialInfo(ProfileSocialInfo $request, $id){
        try{
            $data = $request->validated();
            EmpSocialInfo::updateOrInsert(['employee_id' => $id], $data);

            return redirect()->route('employees')->with(['success_msg' => 'Social information has been updated.']);
        }
        catch (\Exception $e) {
            return redirect()->back()->withInput()->with(['error_msg' => 'Something went wrong, failed to update.']);
        }
    }

    public function updateMealSubscription(ProfleMealSubscription $request, $id){

        try{
            $data = $request->validated();
            EmpMealSubscription::updateOrInsert(['employee_id' => $id], $data);

            return redirect()->route('employees')->with(['success_msg' => 'Meal subscription has been updated.']);
        }
        catch (\Exception $e) {
            return redirect()->back()->withInput()->with(['error_msg' => 'Something went wrong, failed to update.']);
        }
    }

    public function delete($id)
    {
        return redirect()->route('employees')->with(['success_msg' => 'Employee has been deleted.']);
    }
}
