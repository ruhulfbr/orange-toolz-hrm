<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Employee;

class AuthController extends Controller
{
    public function index()
    {

        if (Auth::check()) {
            return redirect()->route('home');
        }

        $pageConfigs = ['blankPage' => true];
        return view('auth.login', ['pageConfigs' => $pageConfigs]);
    }

    public function login(Request $request)
    {
        if (Auth::check()) {
            return redirect()->route('home');
        }

        $credentials = $request->validate([
            'username' => 'required|string'
        ]);

        $field = filter_var($request->username, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

        if (Auth::attempt([$field => $request->username, 'password' => $request->password, 'status' => 'active'])) {
            $request->session()->regenerate();


            $user = Employee::find(auth()->user()->id);
            $user->last_login = date('Y-m-d H:i:s');
            $user->save();

            // Authentication was successful
            // Redirect to the intended URL or a default location
            return redirect()->intended('/')->with(['login-success' => 'yes']); 
        }

        // Authentication failed
        return back()->withInput()->withErrors(['username' => 'Invalid credentials']);
    }

    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect()->route('login');
    }
}
