<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Team;
use App\Models\Employee;

class TeamController extends Controller
{
    private $_VIEW_PATH = "team.";

    public function index(Request $request)
    {
        $data = [
            'title'       => 'Team',
            'breadcrumbs' => [['name' => 'Team']],
            'team'        => auth()->user()->empTeam
        ];

        return view($this->_VIEW_PATH.'index', $data);
    }
}
