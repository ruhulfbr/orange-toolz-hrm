<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Holiday;

class HolidaysController extends Controller
{
    private $_VIEW_PATH = "holiday.";

    public function index(Request $request)
    {
        $data = [
            'title' => 'Holidays',
            'breadcrumbs' => [['name' => 'Holidays']],
            'holidays' => Holiday::whereYear('date', date('Y'))->oldest('date')->get()
        ];

        return view($this->_VIEW_PATH.'index', $data);
    }
}
