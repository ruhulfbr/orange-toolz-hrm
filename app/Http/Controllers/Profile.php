<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ProfilePersonalInfo;
use App\Http\Requests\ProfileOfficialInfo;
use App\Http\Requests\ProfileSocialInfo;
use App\Http\Requests\ProfleMealSubscription;

use App\Models\EmpProfile;
use App\Models\EmpSocialInfo;
use App\Models\EmpMealSubscription;

class Profile extends Controller
{

    private $_VIEW_PATH = "profile.";

    public function index()
    {
        $data = [
            'title' => 'Profile',
            'breadcrumbs' => [['name' => 'Profile']],
            'employee'    => auth()->user()
        ];

        return view($this->_VIEW_PATH.'index', $data);
    }

    public function edit()
    {
        $data = [
            'title' => 'Edit Profile',
            'breadcrumbs' => [['name' => 'Profile', 'link' => route('profile')], ['name' => 'Edit']],
            'user' => auth()->user(),
            'employee' => auth()->user()
        ];

        return view($this->_VIEW_PATH.'edit', $data);
    }

    public function updatePersonalInfo(ProfilePersonalInfo $request){
        $employee_id = auth()->user()->id;

        try{
            $data = $request->validated();
            EmpProfile::updateOrInsert(['employee_id' => $employee_id], $data);

            return redirect()->route('profile')->with(['success_msg' => 'Personal information has been updated.']);
        }
        catch (\Exception $e) {
            return redirect()->back()->withInput()->with(['error_msg' => 'Something went wrong, failed to update.']);
        }
    }

    public function updateOfficialInfo(ProfileOfficialInfo $request){
        $employee_id = auth()->user()->id;

        try{
            $data = $request->validated();
            EmpOfficialInfo::updateOrInsert(['employee_id' => $employee_id], $data);

            return redirect()->route('profile')->with(['success_msg' => 'Official information has been updated.']);
        }
        catch (\Exception $e) {
            return redirect()->back()->withInput()->with(['error_msg' => 'Something went wrong, failed to update.']);
        }
    }

    public function updateSocialInfo(ProfileSocialInfo $request){
        $employee_id = auth()->user()->id;

        try{
            $data = $request->validated();
            EmpSocialInfo::updateOrInsert(['employee_id' => $employee_id], $data);

            return redirect()->route('profile')->with(['success_msg' => 'Social information has been updated.']);
        }
        catch (\Exception $e) {
            return redirect()->back()->withInput()->with(['error_msg' => 'Something went wrong, failed to update.']);
        }
    }

    public function updateMealSubscription(ProfleMealSubscription $request){
        $employee_id = auth()->user()->id;

        try{
            $data = $request->validated();
            EmpMealSubscription::updateOrInsert(['employee_id' => $employee_id], $data);

            return redirect()->route('profile')->with(['success_msg' => 'Meal subscription has been updated.']);
        }
        catch (\Exception $e) {
            return redirect()->back()->withInput()->with(['error_msg' => 'Something went wrong, failed to update.']);
        }
    }
}
