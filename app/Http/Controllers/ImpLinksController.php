<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ImportantLink;

class ImpLinksController extends Controller
{
    private $_VIEW_PATH = "important-link.";

    public function index(Request $request)
    {


        $data = [
            'title' => 'Important Links',
            'breadcrumbs' => [['name' => 'Important Links']],
            'links' => ImportantLink::all()
        ];

        return view($this->_VIEW_PATH.'index', $data);
    }
}
