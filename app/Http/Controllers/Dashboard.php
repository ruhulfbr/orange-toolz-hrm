<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Dashboard extends Controller
{

    private $_VIEW_PATH = "dashboard.";

    public function index()
    {
        $data = [
            'title' => 'Dashboard'
        ];

        return view($this->_VIEW_PATH.'index', $data);
    }
}
