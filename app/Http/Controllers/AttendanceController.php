<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Attendance;
use App\Models\Employee;
use Carbon\Carbon;

class AttendanceController extends Controller
{
    private $_VIEW_PATH = "attendance.";

    public function index(Request $request)
    {
        $data = [
            'title' => 'Attendance Record',
            'breadcrumbs' => [['name' => 'Attendance']],
            'attendances' => $this->getAttendanceData($request)
        ];

        return view($this->_VIEW_PATH.'index', $data);
    }

    private function getAttendanceData(Request $request){
        $employee_id = auth()->user()->id;

        $year = $request->att_month ? date('Y', strtotime($request->att_month)) : date('Y');
        $month = $request->att_month ? date('m', strtotime($request->att_month)) : date('m');

        $startDate = Carbon::create($year, $month, 1)->startOfMonth();
        $endDate = Carbon::create($year, $month, 1)->endOfMonth();

        $attendances = Attendance::where('employee_id', $employee_id)
                        ->whereBetween('in_time', [$startDate, $endDate])
                        ->orderBy('in_time')
                        ->get();

        $dayWiseData = [];

        if(!empty($attendances) && count($attendances) > 0){
            foreach ($attendances->toArray() as $key => $row) {
                $date = date('Y-m-d', strtotime($row['in_time']));
                $dayWiseData[$date] = $row;
            }
        }

        // Config Values
        $weeendIndex = [env('W_1', 5), env('W_2', 6)];
        $intimeLimit = strtotime(env('IN_TIME_LIMT', '10:15:59'));
        $lateinLimit = strtotime(env('LATE_IN_LIMT', '11:00:59'));
        $outtimeLimit  = strtotime(env('OUT_TIME_LIMT', '19:00:00'));
        $earlyoutLimit = strtotime(env('EARLY_OUT_LIMT', '17:00:00'));

        $month_last_day = $endDate->format('j');
        if( $endDate->format('Y-m') == date('Y-m')){
            $month_last_day = now()->format('j');
        }

        // Summary
        $summary = new \stdClass();
        $summary->total_day = $month_last_day;
        $summary->upcoming = $month_last_day - date('j');
        $summary->weakends = 0;
        $summary->presents = 0;
        $summary->late_in = 0;
        $summary->early_out = 0;

        // Date wise attendance
        $date_wise_att = [];

        for( $i=1; $i<=$month_last_day; $i++ ){

            $date = date('Y-m-d', strtotime($year.'-'.$month.'-'.$i));
            $day_index = date('w', strtotime($date));
            $isWeekend = in_array($day_index, $weeendIndex);

            $att = new \stdClass();
            $att->status = 'absent'; 
            $att->status_class = 'text-danger';
            $att->late_in = 'no';
            $att->early_out = 'no';
            $att->half_day = 'no';

            if( isset($dayWiseData[$date]) ){

                $summary->presents += 1;

                $att = (object) $dayWiseData[$date];
                $att->status = 'present';
                $att->status_class = 'text-success';
                $att->in_time_f = date('h:i A', strtotime($att->in_time));
                $att->out_time_f = "";
                $att->half_day = 'no';

                $intime = date('H:i:s', strtotime($att->in_time));
                if(strtotime($intime) > $intimeLimit){
                    $att->late_in = 'yes';

                    $summary->late_in += 1; //Sumary late in
                }
                else{
                    $att->late_in = 'no';
                }

                if(strtotime($intime) > $lateinLimit){
                    $att->half_day = 'yes';
                }

                if( !empty($att->out_time) ){
                    $att->out_time_f = date('h:i A', strtotime($att->out_time));

                    $out_time = date('H:i:s', strtotime($att->out_time));
                    $out_time_int = strtotime($out_time);

                    

                    if($out_time_int < $outtimeLimit ){
                        $att->early_out = 'yes';
                        $summary->early_out += 1; //Sumary early out
                    }
                    else{
                        $att->early_out = 'no';
                    }

                    if($att->half_day == 'no' && $out_time_int < $earlyoutLimit){
                        $att->half_day = 'yes';
                    }

                    $att->duration = $this->calculateDuration($att->in_time, $att->out_time);
                }
            }

            if($isWeekend){
                $att->status = 'weekend';
                $att->status_class = 'text-dark';

                $summary->weakends += 1;
            }
         
            $att->day_name = date('l', strtotime($date));

            $date_wise_att[$date] = $att;

        }

        return $date_wise_att;
    }

    private function calculateDuration($inTime, $outTime) {
        $format = 'Y-m-d H:i:s';
        
        $inDateTime = \DateTime::createFromFormat($format, $inTime);
        $outDateTime = \DateTime::createFromFormat($format, $outTime);

        $interval = $inDateTime->diff($outDateTime);

        $hours = str_pad($interval->format('%h'), 2, '0', STR_PAD_LEFT);
        $minutes = str_pad($interval->format('%i'), 2, '0', STR_PAD_LEFT);

        return $hours.":".$minutes;
    }
}
