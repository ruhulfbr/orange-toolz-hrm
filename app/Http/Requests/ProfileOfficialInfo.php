<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProfileOfficialInfo extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'skype' => 'required|string|max:255',
            'gmail' => 'required|email|max:255',
            'gitlab' => 'nullable|string|max:255',
            'github' => 'nullable|string|max:255',
            'nid' => 'required|string|max:20',
            'tin' => 'nullable|string|max:20',
            'bank_name' => 'nullable|string|max:255',
            'bank_account_no' => 'nullable|string|max:20',
        ];
    }
}
