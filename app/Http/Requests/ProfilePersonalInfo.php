<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProfilePersonalInfo extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'gender' => 'required|in:Male,Female,Other',
            'religion' => 'required|string|max:100',
            'birthday' => 'required|date',
            'official_birthday' => 'nullable|date',
            'blood_group' => 'nullable|string',
            'father_name' => 'required|string|max:150',
            'mother_name' => 'required|string|max:150',
            'marital_status' => 'required|in:Single,Married',
            'spouse' => 'nullable|string|max:150',
            'children' => 'nullable|numeric',
            'present_address' => 'required|string|max:300',
            'permanent_address' => 'required|string|max:300',
        ];
    }
}
