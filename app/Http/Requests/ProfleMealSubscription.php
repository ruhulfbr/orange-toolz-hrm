<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProfleMealSubscription extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'breakfast' => 'required|in:yes,no',
            'lunch' => 'required|in:yes,no',
            'beef' => 'required|in:yes,no',
            'fish' => 'required|in:yes,no',
        ];
    }
}
