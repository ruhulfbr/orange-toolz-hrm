<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProfileSocialInfo extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'website' => 'nullable|url|max:255',
            'facebook' => 'required|url|max:255',
            'twitter' => 'nullable|url|max:255',
            'linkedin' => 'required|url|max:255',
            'instagram' => 'nullable|url|max:255',
            'youtube' => 'nullable|url|max:255',
            'tikTok' => 'nullable|url|max:255',
            'gitlab' => 'nullable|url|max:255',
            'github' => 'nullable|url|max:255',
            'gmail' => 'required|email|max:255',
        ];
    }
}
