<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'emp_id' => 'required|string|unique:employees,emp_id',
            'name' => 'required|string',
            'username' => 'required|string|unique:employees,username',
            'email' => 'required|email|unique:employees,email',
            'photo' => 'required|image|mimes:jpeg,png,jpg,gif|dimensions:width=400,height=400|max:2048',
            'password' => 'required|string|confirmed|min:6|max:30',
            'password_confirmation' => 'required|string',
            'phone' => 'required|string|min:11|max:15',
            'designation' => 'required|string',
            'division' => 'required|string',
            'department' => 'nullable|string',
            'sub_department' => 'nullable|string',
            'unit' => 'nullable|string',
            'team' => 'nullable|string',
            'line_manager' => 'nullable|string',
            'joining_date' => 'required|date',
            'confirmed' => 'nullable|in:yes,no',
            'confirmation_date' => 'nullable|date',
            'role' => 'nullable|string'
        ];
    }
}
