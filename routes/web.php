<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\Dashboard;
use App\Http\Controllers\Profile;
use App\Http\Controllers\AttendanceController;
use App\Http\Controllers\HolidaysController;
use App\Http\Controllers\ImpLinksController;
use App\Http\Controllers\TeamController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\LeavesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// routes/web.php

// Authentication routes
Route::get('/login', [AuthController::class, 'index'])->name('login');
Route::post('/login', [AuthController::class, 'login']);

Route::middleware(['login'])->group(function () {
    Route::get('/', [Dashboard::class, 'index'])->name('home');

    Route::prefix('profile')->group(function () {
        Route::get('/', [Profile::class, 'index'])->name('profile');
        Route::get('/edit', [Profile::class, 'edit'])->name('edit-profile');
        Route::post('/update-personal-info', [Profile::class, 'updatePersonalInfo'])->name('profile.personal-info.update');
        Route::post('/update-official-info', [Profile::class, 'updateOfficialInfo'])->name('profile.official-info.update');
        Route::post('/update-social-info', [Profile::class, 'updateSocialInfo'])->name('profile.social-info.update');
        Route::post('/update-meal-subscription', [Profile::class, 'updateMealSubscription'])->name('profile.meal.update');
    });

    Route::prefix('attendance')->group(function () {
        Route::get('/', [AttendanceController::class, 'index'])->name('attendance');
    });

    Route::prefix('employees')->group(function () {
        Route::get('/', [EmployeeController::class, 'index'])->name('employees');
        Route::get('/show/{id}', [EmployeeController::class, 'show'])->name('employee.show');

        Route::get('/create', [EmployeeController::class, 'create'])->name('employee.create');        
        Route::post('/store', [EmployeeController::class, 'store'])->name('employee.store');

        Route::get('/edit/{id}', [EmployeeController::class, 'edit'])->name('employee.edit');        
        Route::post('/edit/{id}', [EmployeeController::class, 'update']);

        Route::post('/update-personal-info/{id}', [EmployeeController::class, 'updatePersonalInfo'])->name('employee.personal-info.update');  
        Route::post('/update-official-info/{id}', [EmployeeController::class, 'updateOfficialInfo'])->name('employee.official-info.update');
        Route::post('/update-social-info/{id}', [EmployeeController::class, 'updateSocialInfo'])->name('employee.social-info.update');
        Route::post('/update-meal-subscription/{id}', [EmployeeController::class, 'updateMealSubscription'])->name('employee.meal.update');  

        Route::get('/delete/{id}', [EmployeeController::class, 'delete'])->name('employee.delete');   

    });

    Route::prefix('team')->group(function () {
        Route::get('/', [TeamController::class, 'index'])->name('team');
    });

    Route::prefix('leaves')->group(function () {
        Route::get('/', [LeavesController::class, 'index'])->name('leaves');
    });

    Route::prefix('holidays')->group(function () {
        Route::get('/', [HolidaysController::class, 'index'])->name('holidays');
    });

    Route::prefix('important-links')->group(function () {
        Route::get('/', [ImpLinksController::class, 'index'])->name('important-links');
    });

    


    // Route::post('/logout', [AuthController::class, 'logout'])->name('logout');
    Route::get('/logout', [AuthController::class, 'logout'])->name('logout');
});
